package mc.frederik.højgaard.potions;

import java.io.File;
import java.io.IOException;

import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerItemConsumeEvent;

import mc.frederik.højgaard.main;
import mc.frederik.højgaard.api.api;



public class potion {
	

	  @EventHandler
	  public static void PotionEvent(PlayerItemConsumeEvent event){
		 if(event.getItem().getType() == Material.POTION){
				File folder = new File(main.instance.getDataFolder().getPath(), "potions/");
				for(File names : folder.listFiles()){
				File file = new File(main.instance.getDataFolder().getPath(), "potions/" + names.getName());
				YamlConfiguration config = YamlConfiguration.loadConfiguration(file);
				 Player p = event.getPlayer();
				File playerfile = new File(main.instance.getDataFolder().getPath(), "spillere/" + p.getUniqueId() + ".yml");
				YamlConfiguration playerconfig = YamlConfiguration.loadConfiguration(playerfile);
				
				if(event.getItem().getItemMeta().getDisplayName().equals(config.getString("Potion.Name").replace("&", "§"))){
			
			 Location loc = p.getLocation();
			 loc.setY(p.getLocation().getY() + 2);
			 p.playEffect(loc, Effect.HEART, 20);
			 p.playSound(p.getLocation(), Sound.ENTITY_GENERIC_DRINK, 3, 2);
		        
			     api api = new api();
		  		 int heal = playerconfig.getInt("Player." + p.getUniqueId() + ".HP") + config.getInt("Potion.HP");
				 if(playerconfig.getInt("Player." + p.getUniqueId() + ".HP") > 0 && playerconfig.getInt("Player." + p.getUniqueId() + ".HP") < api.getMaxHP(p)){
					 if(heal > api.getMaxHP(p)){
						 playerconfig.set("Player." + p.getUniqueId() + ".HP", api.getMaxHP(p));
						 try {
							playerconfig.save(playerfile);
						} catch (IOException e) {
							e.printStackTrace();
						}
					 }else{
						 playerconfig.set("Player." + p.getUniqueId() + ".HP", heal);
						 try {
							playerconfig.save(playerfile);
						} catch (IOException e) {
							e.printStackTrace();
						} 
					 }
					
				 }
				 p.getInventory().removeItem(p.getInventory().getItemInMainHand());
				 p.updateInventory();	 
			     event.setCancelled(true);	
		 }
				}
	  }
	  
	  }
	
}

package mc.frederik.højgaard.horse;

import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Horse;
import org.bukkit.entity.Horse.Color;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import mc.frederik.højgaard.main;

public class horse implements Listener {

	@EventHandler
	public static void HorseItem(PlayerInteractEvent event){
		Player p = event.getPlayer();
		
		if(event.getAction() == Action.RIGHT_CLICK_BLOCK){
		
		if(event.getItem() == null){
			return;
		}else{
		
			
			if(p.getInventory().getItemInMainHand().getItemMeta().getDisplayName() == null){
				return;
			} else{
			
		if(event.getItem().getType() == Material.SADDLE && event.getItem().getItemMeta().getDisplayName().equals("§aHest §8(§7Tier 1§8)")){
		
				
				if(!main.horsedata.containsKey(p)){
					p.sendMessage("§b§lAzarath §8§l» §aDu har nu spawnet din hest!");
					Horse horse = (Horse)p.getWorld().spawnEntity(p.getLocation(), EntityType.HORSE);
					horse.setCustomName(p.getName() + "§s hest");
					horse.setCustomNameVisible(true);
					horse.setOwner(p);
					horse.setMaxHealth(20.0D);
					horse.setHealth(20.0D);
					horse.setCanPickupItems(true);
	                horse.setBreed(false);
					horse.setTamed(true);
					horse.setColor(Color.BROWN);
					horse.setAdult();
					horse.setJumpStrength(0.5D);
					horse.getInventory().setSaddle(new ItemStack(Material.SADDLE));
					main.horsedata.put(p, horse.getUniqueId());
					return;
					
				}else{	
					for(Entity e : p.getWorld().getEntities()){
						if(e.getUniqueId().equals(main.horsedata.get(p))){
							e.remove();
						}
					}
						main.horsedata.remove(p);
						p.sendMessage("§b§lAzarath §8§l» §cDu har nu despawnet din hest!");
					
					
				}
			
			} else if(event.getItem().getType() == Material.SADDLE && event.getItem().getItemMeta().getDisplayName().equals("§aHest §8(§7Tier 2§8)")){
				if(!main.horsedata.containsKey(p)){
					p.sendMessage("§b§lAzarath §8§l» §aDu har nu spawnet din hest!");
					Horse horse = (Horse)p.getWorld().spawnEntity(p.getLocation(), EntityType.HORSE);
					horse.setCustomName(p.getName() + "§s hest");
					horse.setCustomNameVisible(true);
					horse.setOwner(p);
					horse.setMaxHealth(20.0D);
					horse.setHealth(20.0D);
					horse.setCanPickupItems(true);
	                horse.setBreed(false);
					horse.setTamed(true);
					horse.setColor(Color.GRAY);
					horse.setAdult();
					horse.setJumpStrength(0.71D);
					horse.getInventory().setSaddle(new ItemStack(Material.SADDLE));
					main.horsedata.put(p, horse.getUniqueId());
					return;
					
				}else{	
					for(Entity e : p.getWorld().getEntities()){
						if(e.getUniqueId().equals(main.horsedata.get(p))){
							e.remove();
						}
					}
						main.horsedata.remove(p);
						p.sendMessage("§b§lAzarath §8§l» §cDu har nu despawnet din hest!");
					
					
				}
			
			
			} else if(event.getItem().getType() == Material.SADDLE && event.getItem().getItemMeta().getDisplayName().equals("§aHest §8(§7Tier 3§8)")){
				if(!main.horsedata.containsKey(p)){
					p.sendMessage("§b§lAzarath §8§l» §aDu har nu spawnet din hest!");
					Horse horse = (Horse)p.getWorld().spawnEntity(p.getLocation(), EntityType.HORSE);
					horse.setCustomName(p.getName() + "§s hest");
					horse.setCustomNameVisible(true);
					horse.setOwner(p);
					horse.setMaxHealth(20.0D);
					horse.setHealth(20.0D);
					horse.setCanPickupItems(true);
	                horse.setBreed(false);
					horse.setTamed(true);
					horse.setColor(Color.BLACK);
					horse.setAdult();
					horse.setJumpStrength(0.84D);
					horse.getInventory().setSaddle(new ItemStack(Material.SADDLE));
					main.horsedata.put(p, horse.getUniqueId());
					return;
					
				}else{	
					for(Entity e : p.getWorld().getEntities()){
						if(e.getUniqueId().equals(main.horsedata.get(p))){
							e.remove();
						}
					}
						main.horsedata.remove(p);
						p.sendMessage("§b§lAzarath §8§l» §cDu har nu despawnet din hest!");
					
					
				}
				
				
			} else if(event.getItem().getType() == Material.SADDLE && event.getItem().getItemMeta().getDisplayName().equals("§aHest §8(§7Tier 4§8)")){
				if(!main.horsedata.containsKey(p)){
					p.sendMessage("§b§lAzarath §8§l» §aDu har nu spawnet din hest!");
					Horse horse = (Horse)p.getWorld().spawnEntity(p.getLocation(), EntityType.HORSE);
					horse.setCustomName(p.getName() + "§s hest");
					horse.setCustomNameVisible(true);
					horse.setOwner(p);
					horse.setMaxHealth(20.0D);
					horse.setHealth(20.0D);
					horse.setCanPickupItems(true);
	                horse.setBreed(false);
					horse.setTamed(true);
					horse.setColor(Color.WHITE);
					horse.setAdult();
					horse.setJumpStrength(1.0D);
					horse.getInventory().setSaddle(new ItemStack(Material.SADDLE));
					main.horsedata.put(p, horse.getUniqueId());
					return;
					
				}else{	
					for(Entity e : p.getWorld().getEntities()){
						if(e.getUniqueId().equals(main.horsedata.get(p))){
							e.remove();
						}
					}
						main.horsedata.remove(p);
						p.sendMessage("§b§lAzarath §8§l» §cDu har nu despawnet din hest!");
					
					
				}
				
				
		}
			}
		}
		}
		
	}
	
}

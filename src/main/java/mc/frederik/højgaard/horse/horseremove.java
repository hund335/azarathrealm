package mc.frederik.højgaard.horse;

import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

import mc.frederik.højgaard.main;

public class horseremove implements Listener {

	
	@EventHandler
	public static void RemoveHorse(PlayerQuitEvent event){
		Player p = event.getPlayer();
		
		if(main.horsedata.containsKey(p)){
			for(Entity e : p.getWorld().getEntities()){
				if(e.getUniqueId().equals(main.horsedata.get(p))){
					e.remove();
				}
			}
				main.horsedata.remove(p);
			
			
			
		}
	}
	
}

package mc.frederik.højgaard.weapons;

import java.io.File;
import java.util.HashMap;
import java.util.Random;

import org.bukkit.Material;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

import mc.frederik.højgaard.main;

public class iron_sword {

	  public static HashMap<String, Long> cooldowns = new HashMap();
	
	@EventHandler
	public static void SwordDamage(EntityDamageByEntityEvent event){
		
		if (event.getDamager() instanceof Player){

	
			File folder = new File(main.instance.getDataFolder().getPath(), "weapons/iron_swords/");
			for(File names : folder.listFiles()){
			File file = new File(main.instance.getDataFolder().getPath(), "weapons/iron_swords/" + names.getName());
			YamlConfiguration config = YamlConfiguration.loadConfiguration(file);
			Player p = (Player)event.getDamager();

			if(p.getInventory().getItemInMainHand().getType() == Material.IRON_SWORD){

				if(p.getInventory().getItemInMainHand().getItemMeta().getDisplayName() != null){
				
			if(p.getInventory().getItemInMainHand().getItemMeta().getDisplayName().equals(config.getString("Weapon.Iron_Sword.Name").replace("&", "§"))){
				
				File file2 = new File(main.instance.getDataFolder().getPath(), "spillere/" + p.getUniqueId() + ".yml");
			    YamlConfiguration config2 = YamlConfiguration.loadConfiguration(file2);
			    
			    if(config2.getString("Player." + p.getUniqueId() + ".Class").equals(config.getString("Weapon.Iron_Sword.Class"))){
			    
			    if(config2.getInt("Player." + p.getUniqueId() + ".Level") < config.getInt("Weapon.Iron_Sword.Level")){
			    	p.sendMessage("§b§lAzarath §8§l» §cDu skal være level " + config.getInt("Weapon.Iron_Sword.Level") + " eller over, for at kunne bruge dette våben!");
					event.setCancelled(true);
					return;
			    }else{
				 int cooldownTime1 = config.getInt("Weapon.Iron_Sword.Attack_Speed");
			      if (cooldowns.containsKey(p.getName()))
			      {
			        long secondsLeft = ((Long)cooldowns.get(p.getName())).longValue() / 1000L + cooldownTime1 - System.currentTimeMillis() / 1000L;
			        if (secondsLeft > 0L)
			        {
			          event.setCancelled(true);
			          return;
			        }
			      }
			      cooldowns.put(p.getName(), Long.valueOf(System.currentTimeMillis()));
		          
				
				Random calc = new Random();
				int atk = calc.nextInt(config.getInt("Weapon.Iron_Sword.MaxDamage") - config.getInt("Weapon.Iron_Sword.MinDamage")) + config.getInt("Weapon.Iron_Sword.MinDamage");
				event.setDamage(atk);
				p.sendMessage("Damage: " + event.getDamage());
			}
			}else{
		    	p.sendMessage("§b§lAzarath §8§l» §cKun " + config.getString("Weapon.Iron_Sword.Class") + " classet kan bruge dette våben!");
				event.setCancelled(true);
				return;
			}
			}
				}
			}else{
				return;
			}
				
		}
			
		}
	}
	
}

package mc.frederik.højgaard.weapons;

import java.io.File;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import org.bukkit.Material;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import mc.frederik.højgaard.main;
import mc.frederik.højgaard.api.mysql;
import net.minecraft.server.v1_12_R1.EnumHand;
import net.minecraft.server.v1_12_R1.EnumItemSlot;
import net.minecraft.server.v1_12_R1.Item;
import net.minecraft.server.v1_12_R1.PacketPlayInBlockPlace;
import net.minecraft.server.v1_12_R1.PacketPlayOutEntityEquipment;



public class bow {
	
	 public static HashMap<String, Long> cooldowns = new HashMap();
	
	 @SuppressWarnings("unchecked")
	public void getBow(Player p, String id) {
	
		 Connection connect = null;
		 ResultSet res = null;
		 
		 try{
		 
		 connect = main.connection.getConnection();		 
		 
		res = connect.prepareStatement("SELECT * FROM weapons_bows WHERE id='"+ id +"'").executeQuery();
		
		if(res.next()){
		 
    		ItemStack bow = new ItemStack(Material.BOW);
    		 ItemMeta bowmeta = bow.getItemMeta();
    		bowmeta.setDisplayName(res.getString(2).replace("&", "§"));
    	      if (res.getBoolean(9) == true)
    	      {
    	        
    	    	  @SuppressWarnings("rawtypes")
				ArrayList lore = new ArrayList();
    	    
    	    
    	    	  lore.add("§7Kræver level: §6" + res.getInt(4));
    	    	  lore.add("§7Værdig: " + res.getString(10).replace("&", "§"));
    	    	  lore.add("§f");
    	    	  lore.add("§7Damage: §6" + res.getInt(6) + " §f- §6" + res.getInt(5));
                 // lore.add(res.getString(10).replace("&", "§").replace("%MinDamage%", "" + res.getInt(6)).replace("%MaxDamage%", "" + res.getInt(5)).replace("%Level%", "" + res.getInt(4)));
    	          bowmeta.setLore(lore);
    	        }
    	      
    		bow.setItemMeta(bowmeta);
    		 p.getInventory().addItem(bow);
    		 p.updateInventory();
    		 
    		 res.close();
    	    
    	  }else{
    	  p.sendMessage("§b§lAzarath §8§l» §cFejl: denne bow ser ikke ud til at findes!"); 
    	  }
		
	 }catch(SQLException e){
		 e.printStackTrace();
	 } finally{
		 
		 if(connect != null){
			 try {
				connect.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		 }
		 if(res != null){
			 try {
				res.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		 }
	 }
		 
	 }
	 
	 
	@EventHandler
	public static void BowSooting(PlayerInteractEvent event) {
		Player p = (Player)event.getPlayer();
		
		if(p.getInventory().getItemInMainHand().getType() == Material.BOW){
				
		if(event.getAction() == Action.RIGHT_CLICK_AIR){
		//net.minecraft.server.v1_12_R1.PacketPlayInBlockPlace
			
		//	PacketPlayInBlockPlace pb = new PacketPlayInBlockPlace(EnumHand.MAIN_HAND);
			//PacketPlayOutEntityEquipment pb2 = new PacketPlayOutEntityEquipment(	((CraftPlayer) p).getHandle().getId(), EnumItemSlot.MAINHAND, new net.minecraft.server.v1_12_R1.ItemStack(Item.getById(261)));
		//	((CraftPlayer) p).getHandle().playerConnection.sendPacket(pb2);


			
			if(p.getInventory().getItemInMainHand().getItemMeta().getDisplayName() == null){
				event.setCancelled(true);
				return;
			}else{
			
				event.setCancelled(true);
             Connection connect = null;
             ResultSet res = null; 
             
             try{
             connect = main.connection.getConnection();
             res = connect.prepareStatement("SELECT * FROM weapons_bows WHERE name='"+ p.getItemInHand().getItemMeta().getDisplayName().replace("§", "&") +"'").executeQuery();
             
		if(res.next()){
		
				mysql ms = new mysql();
				
			    
			    if(ms.GetString(p, 3).equals(res.getString(3))){
			    
			    if(ms.GetInt(p, 2) < res.getInt(4)){
			    	p.sendMessage("§b§lAzarath §8§l» §cDu skal være level " + res.getInt(4) + " eller over, for at kunne bruge dette våben!");
					event.setCancelled(true);
					return;
			    }else{
				
				 int cooldownTime1 = res.getInt(8);
			      if (cooldowns.containsKey(p.getName()))
			      {
			        long secondsLeft = ((Long)cooldowns.get(p.getName())).longValue() / 1000L + cooldownTime1 - System.currentTimeMillis() / 1000L;
			        if (secondsLeft > 0L)
			        {
			          event.setCancelled(true);
			          return;
			        }
			      }
			      cooldowns.put(p.getName(), Long.valueOf(System.currentTimeMillis()));
		          
		          
				
				Arrow arrow = p.launchProjectile(Arrow.class);		
				arrow.setVelocity(p.getEyeLocation().getDirection().multiply(res.getInt(7)));
	
				Random calc = new Random();
				int atk = calc.nextInt(res.getInt(5) - res.getInt(6)) + res.getInt(6);
				arrow.spigot().setDamage(atk);
			    p.sendMessage("Arrow damage : " + arrow.spigot().getDamage());
				//PacketPlayInBlockPlace pb = new PacketPlayInBlockPlace(EnumHand.MAIN_HAND);
				//((CraftPlayer)p).getHandle().playerConnection.a(pb);
		
			}
			    }else{
			    	p.sendMessage("§b§lAzarath §8§l» §cKun " + res.getString(3) + " classet kan bruge dette våben!");
					event.setCancelled(true);
					return;
				}
		}
			
             
			
             }catch(SQLException e){
            	 e.printStackTrace();
            	 } finally{
            		 
            		 if(connect != null){
            			 try {
							connect.close();
						} catch (SQLException e) {
							e.printStackTrace();
						}
            		 }
            		 if(res != null){
            			 try {
							res.close();
						} catch (SQLException e) {
							e.printStackTrace();
						}
            		 }
            	 }
             }
			
		
			
			
			
		}else if(event.getAction() == Action.RIGHT_CLICK_BLOCK){
			event.setCancelled(true);
			return;
		}
			}
	}
	
}
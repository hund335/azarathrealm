package mc.frederik.højgaard.weapons;

import java.io.File;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import mc.frederik.højgaard.main;
import mc.frederik.højgaard.api.mysql;
import net.minecraft.server.v1_12_R1.EnumParticle;
import net.minecraft.server.v1_12_R1.PacketPlayOutEntityDestroy;
import net.minecraft.server.v1_12_R1.PacketPlayOutWorldParticles;


public class wand {

	private static EnumParticle e;
	  public static HashMap<String, Long> cooldowns = new HashMap();

		 @SuppressWarnings("unchecked")
			public void getWand(Player p, String id) {
			
				 Connection connect = null;
				 ResultSet res = null;
				 
				 try{
				 
				 connect = main.connection.getConnection();		 
				 
				res = connect.prepareStatement("SELECT * FROM weapons_wands WHERE id='"+ id +"'").executeQuery();
				
				if(res.next()){
				 
		    		ItemStack wand = new ItemStack(Material.STICK);
		    		 ItemMeta wandmeta = wand.getItemMeta();
		    		wandmeta.setDisplayName(res.getString(2).replace("&", "§"));
		    	      if (res.getBoolean(10) == true)
		    	      {
		    	        
		    	    	  @SuppressWarnings("rawtypes")
						ArrayList lore = new ArrayList();
		    	    
		    	    
		    	    	  lore.add("§7Kræver level: §6" + res.getInt(5));
		    	    	  lore.add("§7Værdig: " + res.getString(11).replace("&", "§"));
		    	    	  lore.add("§f");
		    	    	  lore.add("§7Damage: §6" + res.getInt(7) + " §f- §6" + res.getInt(6));
		                 // lore.add(res.getString(10).replace("&", "§").replace("%MinDamage%", "" + res.getInt(6)).replace("%MaxDamage%", "" + res.getInt(5)).replace("%Level%", "" + res.getInt(4)));
		    	          wandmeta.setLore(lore);
		    	        }
		    	      
		    		 wand.setItemMeta(wandmeta);
		    		 p.getInventory().addItem(wand);
		    		 p.updateInventory();
		    		 
		    		 res.close();
		    	    
		    	  }else{
		    	  p.sendMessage("§b§lAzarath §8§l» §cFejl: denne wand ser ikke ud til at findes!"); 
		    	  }
				
			 }catch(SQLException e){
				 e.printStackTrace();
			 } finally{
				 
				 if(connect != null){
					 try {
						connect.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				 }
				 if(res != null){
					 try {
						res.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				 }
			 }
				 
			 }
	  
	  
	  
	  
	  
	@EventHandler
	public static void WandSooting(PlayerInteractEvent event){
		Player p = (Player)event.getPlayer();
		
		if(p.getInventory().getItemInHand().getType() == Material.STICK){
		if(event.getAction() == Action.LEFT_CLICK_AIR){
			if(p.getInventory().getItemInHand().getItemMeta().getDisplayName() == null){
				event.setCancelled(true);
				return;
			}else{				
				 Connection connect = null;
                 ResultSet res = null;
				 
				 try{
				 
				 connect = main.connection.getConnection();		 
				 
				res = connect.prepareStatement("SELECT * FROM weapons_wands WHERE name='"+ p.getItemInHand().getItemMeta().getDisplayName().replace("§", "&") +"'").executeQuery();
				
				if(res.next()){
					 mysql ms = new mysql();


			    
			    if(ms.GetString(p, 3).equals(res.getString(3))){
			    
			    if(ms.GetInt(p, 2) < res.getInt(5)){
			    	p.sendMessage("§b§lAzarath §8§l» §cDu skal være level " + res.getInt(5) + " eller over, for at kunne bruge dette våben!");
					event.setCancelled(true);
					return;
			    }else{
				
				 int cooldownTime1 = res.getInt(9);
			      if (cooldowns.containsKey(p.getName()))
			      {
			        long secondsLeft = ((Long)cooldowns.get(p.getName())).longValue() / 1000L + cooldownTime1 - System.currentTimeMillis() / 1000L;
			        if (secondsLeft > 0L)
			        {
			          event.setCancelled(true);
			          return;
			        }
			      }
			      cooldowns.put(p.getName(), Long.valueOf(System.currentTimeMillis()));
		          
				
				Arrow arrow = p.launchProjectile(Arrow.class);		
				arrow.setVelocity(p.getEyeLocation().getDirection().multiply(res.getInt(8)));
				Random calc = new Random();
				int atk = calc.nextInt(res.getInt(6) - res.getInt(7)) + res.getInt(7);
				//arrow.spigot().setDamage(Math.random()*(config.getInt("Weapon.Wand.MaxDamage")-config.getInt("Weapon.Wand.MinDamage")+config.getInt("Weapon.Wand.MinDamage")));
				arrow.spigot().setDamage(atk);
				PacketPlayOutEntityDestroy ra = new  PacketPlayOutEntityDestroy(arrow.getEntityId());
				for(Player players : Bukkit.getOnlinePlayers()){		
				((CraftPlayer) players).getHandle().playerConnection.sendPacket(ra);
				int id = res.getInt(4);

			Bukkit.getScheduler().scheduleSyncRepeatingTask(main.instance, new Runnable(){
		  @Override
          public void run() {
			  			
				Location loc = arrow.getLocation();

		

				PacketPlayOutWorldParticles	pp = new PacketPlayOutWorldParticles(e.a(id), true, (float) loc.getX(), (float) loc.getY(), (float) loc.getZ(), 0, 0, 0, 0, 15, null);
		
			if(arrow.isOnGround() || arrow.isDead() || arrow == null){ 
				return;
				}else{
			  		((CraftPlayer) players).getHandle().playerConnection.sendPacket(pp);
				
		  
      
				
		  
				}
				
				}
		  
			
				
				
				
			 
			
	 }, 0,1);
			
			    
				
			    }
			    
			    
			    }
			    
			    }else{
			    	p.sendMessage("§b§lAzarath §8§l» §cKun " + res.getString(3) + " classet kan bruge dette våben!");
					event.setCancelled(true);
					return;
			    }
				 }
				 }catch(SQLException e){
					 e.printStackTrace();
				 } finally{
					 
					 if(connect != null){
						 try {
							connect.close();
						} catch (SQLException e) {
							e.printStackTrace();
						}
					 }
					 if(res != null){
						 try {
							res.close();
						} catch (SQLException e) {
							e.printStackTrace();
						}
					 }
				 }
			}
		}
		}
	}
				 
					 
				 
		
		
	@EventHandler
    public static void onland(ProjectileHitEvent event){
		Arrow arrow = (Arrow)event.getEntity();
				arrow.remove();
	}
	

	@EventHandler
    public static void onShoot(ProjectileLaunchEvent event){

	}
}

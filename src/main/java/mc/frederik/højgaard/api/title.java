package mc.frederik.højgaard.api;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

import net.minecraft.server.v1_12_R1.IChatBaseComponent;
import net.minecraft.server.v1_12_R1.PacketPlayOutTitle;
import net.minecraft.server.v1_12_R1.PlayerConnection;

public class title
{
  public static void broadcastAllTitle(Integer fadeIn, Integer stay, Integer fadeOut, String text)
  {
    for (Player p : Bukkit.getServer().getOnlinePlayers()) {
      playTitle(p, fadeIn, stay, fadeOut, text);
    }
  }
  
  public static void broadcastAllSubTitle(Integer fadeIn, Integer stay, Integer fadeOut, String text)
  {
    for (Player p : Bukkit.getServer().getOnlinePlayers()) {
      playTitle(p, fadeIn, stay, fadeOut, null, text);
    }
  }
  
  public static void broadcastAllFullTitle(Integer fadeIn, Integer stay, Integer fadeOut, String subtitle, String title)
  {
    for (Player p : Bukkit.getServer().getOnlinePlayers()) {
      playTitle(p, fadeIn, stay, fadeOut, title, subtitle);
    }
  }
  
  public static void playTitle(Player player, Integer fadeIn, Integer stay, Integer fadeOut, String text)
  {
    playTitle(player, fadeIn, stay, fadeOut, text, null);
  }
  
  public static void playSubTitle(Player player, Integer fadeIn, Integer stay, Integer fadeOut, String text)
  {
    playTitle(player, fadeIn, stay, fadeOut, null, text);
  }
  
  public static void playFullTitle(Player player, Integer fadeIn, Integer stay, Integer fadeOut, String title, String subtitle)
  {
    playTitle(player, fadeIn, stay, fadeOut, title, subtitle);
  }
  
  public static void playFullTitle2(LivingEntity player, Integer fadeIn, Integer stay, Integer fadeOut, String title, String subtitle)
  {
    playTitle2(player, fadeIn, stay, fadeOut, title, subtitle);
  }
  
  public static void playTitle(Player player, Integer fadeIn, Integer stay, Integer fadeOut, String title, String subtitle)
  {
    PlayerConnection connection = ((CraftPlayer)player).getHandle().playerConnection;
    
    PacketPlayOutTitle packetPlayOutTimes = new PacketPlayOutTitle(PacketPlayOutTitle.EnumTitleAction.TIMES, null, fadeIn.intValue(), stay.intValue(), fadeOut.intValue());
    connection.sendPacket(packetPlayOutTimes);
    if (subtitle != null)
    {
      subtitle = subtitle.replaceAll("%player%", player.getDisplayName());
      subtitle = ChatColor.translateAlternateColorCodes('&', subtitle);
      IChatBaseComponent titleSub = IChatBaseComponent.ChatSerializer.a("{\"text\": \"" + subtitle + "\"}");
      PacketPlayOutTitle packetPlayOutSubTitle = new PacketPlayOutTitle(PacketPlayOutTitle.EnumTitleAction.SUBTITLE, titleSub);
      connection.sendPacket(packetPlayOutSubTitle);
    }
    if (title != null)
    {
      title = title.replaceAll("%player%", player.getDisplayName());
      title = ChatColor.translateAlternateColorCodes('&', title);
      IChatBaseComponent titleMain = IChatBaseComponent.ChatSerializer.a("{\"text\": \"" + title + "\"}");
      PacketPlayOutTitle packetPlayOutTitle = new PacketPlayOutTitle(PacketPlayOutTitle.EnumTitleAction.TITLE, titleMain);
      connection.sendPacket(packetPlayOutTitle);
    }
  }
  
  public static void playTitle2(LivingEntity player, Integer fadeIn, Integer stay, Integer fadeOut, String title, String subtitle)
  {
    PlayerConnection connection = ((CraftPlayer)player).getHandle().playerConnection;
    
    PacketPlayOutTitle packetPlayOutTimes = new PacketPlayOutTitle(PacketPlayOutTitle.EnumTitleAction.TIMES, null, fadeIn.intValue(), stay.intValue(), fadeOut.intValue());
    connection.sendPacket(packetPlayOutTimes);
    if (subtitle != null)
    {
      subtitle = ChatColor.translateAlternateColorCodes('&', subtitle);
      IChatBaseComponent titleSub = IChatBaseComponent.ChatSerializer.a("{\"text\": \"" + subtitle + "\"}");
      PacketPlayOutTitle packetPlayOutSubTitle = new PacketPlayOutTitle(PacketPlayOutTitle.EnumTitleAction.SUBTITLE, titleSub);
      connection.sendPacket(packetPlayOutSubTitle);
    }
    if (title != null)
    {
      title = ChatColor.translateAlternateColorCodes('&', title);
      IChatBaseComponent titleMain = IChatBaseComponent.ChatSerializer.a("{\"text\": \"" + title + "\"}");
      PacketPlayOutTitle packetPlayOutTitle = new PacketPlayOutTitle(PacketPlayOutTitle.EnumTitleAction.TITLE, titleMain);
      connection.sendPacket(packetPlayOutTitle);
    }
  }
}

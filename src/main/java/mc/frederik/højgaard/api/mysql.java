package mc.frederik.højgaard.api;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.TimeZone;

import org.bukkit.entity.Player;

import mc.frederik.højgaard.main;


public class mysql {
	
	public int GetInt(Player p, int r){
		 Connection connect = null;
		 ResultSet res = null;
		 int data;
		 try {
			 connect = main.connection.getConnection();
			 
			 
			  res = connect.prepareStatement("SELECT * FROM players WHERE uuid='"+ p.getUniqueId() +"'").executeQuery();
			 if(res.next()){
				 data = res.getInt(r);
        	     res.close();
        	     
        
        			return data;
        		}
			 
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			if(connect != null){
				try {
					connect.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if(res != null){
				try {
					res.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		 return 0;
	 }
	
	

	public String GetString(Player p, int r){
		 Connection connect = null;
		 ResultSet res = null;
		 String data;
		 try {
			 connect = main.connection.getConnection();
			 
			 
			  res = connect.prepareStatement("SELECT * FROM players WHERE uuid='"+ p.getUniqueId() +"'").executeQuery();
			 if(res.next()){
				 data = res.getString(r);
       	     res.close();
       	     
       
       			return data;
       		}
			 
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			if(connect != null){
				try {
					connect.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if(res != null){
				try {
					res.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		 return "error";
	 }
	
	 


	
	public Double GetDouble(Player p, int r){
		 Connection connect = null;
		 ResultSet res = null;
		 double data;
		 try {
			 connect = main.connection.getConnection();
			 
			 
			  res = connect.prepareStatement("SELECT * FROM players WHERE uuid='"+ p.getUniqueId() +"'").executeQuery();
			 if(res.next()){
				 data = res.getDouble(r);
      	     res.close();
      	     
      
      			return data;
      		}
			 
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			if(connect != null){
				try {
					connect.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if(res != null){
				try {
					res.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		 return 0.0;
	 }
	
	/*

	public void Update(Player p, String r, int i){
		
		  Bukkit.getScheduler().runTaskAsynchronously(main.instance, new Runnable() {
	        
	            public void run() {
		try {
			ResultSet res = main.connect.prepareStatement("SELECT * FROM players WHERE uuid='"+ p.getUniqueId() +"'").executeQuery();
		
			
			if(res.next()){
			
				  String update = " update players set " + r + " = ? WHERE uuid='"+ p.getUniqueId() +"'";
				  PreparedStatement ps = main.connect.prepareStatement(update);
				  ps.setInt(1, i);
				  ps.execute();
				  ps.close();
				res.close();
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}
		  });
	}
	*/
	
	
	public void Update(Player p, String r, int i){
		 Connection connect = null;
		 ResultSet res = null;
		 PreparedStatement ps = null;
		 try {
			 connect = main.connection.getConnection();
			res = connect.prepareStatement("SELECT * FROM players WHERE uuid='"+ p.getUniqueId() +"'").executeQuery();
			 
			 if(res.next()){
			 
			 String update = " update players set " + r + " = ? WHERE uuid='"+ p.getUniqueId() +"'";
			  ps = connect.prepareStatement(update);
			  ps.setInt(1, i);
		      ps.execute();
		      ps.close();
		      res.close();
			 }
			 
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			if(connect != null){
				try {
					connect.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if(ps != null){
				try {
					ps.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if(res != null){
				try {
					res.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
	 }
	
	
	public void Update(Player p, String r, String s){
		 Connection connect = null;
		 ResultSet res = null;
		 PreparedStatement ps = null;
		 try {
			 connect = main.connection.getConnection();
			res = connect.prepareStatement("SELECT * FROM players WHERE uuid='"+ p.getUniqueId() +"'").executeQuery();
			 
			 if(res.next()){
			 
			 String update = " update players set " + r + " = ? WHERE uuid='"+ p.getUniqueId() +"'";
			  ps = connect.prepareStatement(update);
			  ps.setString(1, s);
		      ps.execute();
		      ps.close();
		      res.close();
			 }
			 
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			if(connect != null){
				try {
					connect.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if(ps != null){
				try {
					ps.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if(res != null){
				try {
					res.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
	 }
	
	public void Update(Player p, String r, double d){
		 Connection connect = null;
		 ResultSet res = null;
		 PreparedStatement ps = null;
		 try {
			 connect = main.connection.getConnection();
			res = connect.prepareStatement("SELECT * FROM players WHERE uuid='"+ p.getUniqueId() +"'").executeQuery();
			 
			 if(res.next()){
			 
			 String update = " update players set " + r + " = ? WHERE uuid='"+ p.getUniqueId() +"'";
			  ps = connect.prepareStatement(update);
			  ps.setDouble(1, d);
		      ps.execute();
		      ps.close();
		      res.close();
			 }
			 
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			if(connect != null){
				try {
					connect.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if(ps != null){
				try {
					ps.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if(res != null){
				try {
					res.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
	 }
	
	public void Insert(Player p){
		 Connection connect = null;
		 ResultSet res = null;
		 PreparedStatement ps = null;
		 try {
			 connect = main.connection.getConnection();
			res = connect.prepareStatement("SELECT * FROM players WHERE uuid='"+ p.getUniqueId() +"'").executeQuery();
			 
			 if(!res.next()){
	              Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT+1"));
				   
				   int year = cal.get(Calendar.YEAR);
				   int month = cal.get(Calendar.MONTH) + 1;
				   int day = cal.get(Calendar.DAY_OF_MONTH);
				   int hours = cal.get(Calendar.HOUR) + 1;
				   int minutes = cal.get(Calendar.MINUTE);
				   int seconds = cal.get(Calendar.SECOND);
			 
				 String insert = " insert into players (UUID, Level, Class, Gold, XP, HP, MaxHP, Mana, MaxMana, Helmet, Chestplate, Leggings, Boots, FirstJoin, LastOnline, SkillPoints, AtkSkill, HPSkill, DefSkill, SpeedSkill, IP)"
					        + " values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
			    ps = connect.prepareStatement(insert);
			    ps.setString(1, String.valueOf(p.getUniqueId()));
                ps.setInt(2, 1);
                ps.setString(3, "Intet");
			    ps.setInt(4, 0); 
			    ps.setLong(5, 0);
			    ps.setInt(6, 50);
			    ps.setInt(7, 50);
			    ps.setInt(8, 20);
			    ps.setInt(9, 20);
			    ps.setInt(10, 0);
			    ps.setInt(11, 0);
			    ps.setInt(12, 0);
			    ps.setInt(13, 0);
			    ps.setString(14, String.valueOf(day + "/" + month + "/" + year + " - " + hours + ":" + minutes + ":" + seconds)); 
			    ps.setString(15, String.valueOf(day + "/" + month + "/" + year + " - " + hours + ":" + minutes + ":" + seconds)); 
			    ps.setInt(16, 0);
			    ps.setInt(17, 0);
			    ps.setInt(18, 0);
			    ps.setInt(19, 0);
                ps.setInt(20, 0);
                ps.setString(21, String.valueOf(p.getAddress()));
                ps.execute();
                ps.close();
		        ps.execute();
		        ps.close();
		        res.close();
			 }
			 
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			if(connect != null){
				try {
					connect.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if(ps != null){
				try {
					ps.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if(res != null){
				try {
					res.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
	 }
	}
	
	
	/*
	
	public void Insert(Player p){
		
		  Bukkit.getScheduler().runTaskAsynchronously(main.instance, new Runnable() {
		        
	            public void run() {
		
	
		try {
			ResultSet res = main.connect.prepareStatement("SELECT * FROM players WHERE uuid='"+ p.getUniqueId() +"'").executeQuery();
		
			
			if(!res.next()){
				
				   Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT+1"));
				   
				   int year = cal.get(Calendar.YEAR);
				   int month = cal.get(Calendar.MONTH) + 1;
				   int day = cal.get(Calendar.DAY_OF_MONTH);
				   int hours = cal.get(Calendar.HOUR) + 1;
				   int minutes = cal.get(Calendar.MINUTE);
				   int seconds = cal.get(Calendar.SECOND);
			
				  String query = " insert into players (UUID, Level, Class, Gold, XP, HP, MaxHP, Mana, MaxMana, Helmet, Chestplate, Leggings, Boots, FirstJoin, LastOnline, SkillPoints, AtkSkill, HPSkill, DefSkill, SpeedSkill, IP)"
					        + " values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
				  
                PreparedStatement ps = main.connect.prepareStatement(query);
                ps.setString(1, String.valueOf(p.getUniqueId()));
                ps.setInt(2, 1);
                ps.setString(3, "Intet");
			    ps.setInt(4, 0); 
			    ps.setLong(5, 0);
			    ps.setInt(6, 50);
			    ps.setInt(7, 50);
			    ps.setInt(8, 20);
			    ps.setInt(9, 20);
			    ps.setInt(10, 0);
			    ps.setInt(11, 0);
			    ps.setInt(12, 0);
			    ps.setInt(13, 0);
			    ps.setString(14, String.valueOf(day + "/" + month + "/" + year + " - " + hours + ":" + minutes + ":" + seconds)); 
			    ps.setString(15, String.valueOf(day + "/" + month + "/" + year + " - " + hours + ":" + minutes + ":" + seconds)); 
			    ps.setInt(16, 0);
			    ps.setInt(17, 0);
			    ps.setInt(18, 0);
			    ps.setInt(19, 0);
                ps.setInt(20, 0);
                ps.setString(21, String.valueOf(p.getAddress()));
                ps.execute();
                ps.close();
			}
				
		} catch (SQLException e) {
			e.printStackTrace();
		}
	            }
	});
	
	}
	*/


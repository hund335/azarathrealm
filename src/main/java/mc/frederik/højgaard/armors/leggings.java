package mc.frederik.højgaard.armors;

import java.io.File;
import java.io.IOException;

import org.bukkit.Material;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;

import mc.frederik.højgaard.main;

public class leggings implements Listener {

	
	@EventHandler
	public static void LeggingsClick(PlayerInteractEvent event) throws IOException{
		Player p = (Player)event.getPlayer();
		if(event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK){

		if(p.getInventory().getItemInMainHand().getType() == Material.LEATHER_LEGGINGS || p.getInventory().getItemInMainHand().getType() == Material.CHAINMAIL_LEGGINGS || p.getInventory().getItemInMainHand().getType() == Material.GOLD_LEGGINGS
				|| p.getInventory().getItemInMainHand().getType() == Material.IRON_LEGGINGS || p.getInventory().getItemInMainHand().getType() == Material.DIAMOND_LEGGINGS){
			
						File folder = new File(main.instance.getDataFolder().getPath(), "armors/leggings/");
						for(File names : folder.listFiles()){
						File file = new File(main.instance.getDataFolder().getPath(), "armors/leggings/" + names.getName());
						YamlConfiguration config = YamlConfiguration.loadConfiguration(file);
						File playerfile = new File(main.instance.getDataFolder().getPath(), "spillere/" + p.getUniqueId() + ".yml");
						YamlConfiguration playerconfig = YamlConfiguration.loadConfiguration(playerfile);
						if(event.getItem().getItemMeta().getDisplayName() == null){
							return;
						}else
							if(event.getItem().getItemMeta().getDisplayName().equals(config.getString("Armor.Leggings.Name").replace("&", "§"))){						
						if(p.getInventory().getLeggings() == null){
							if(playerconfig.getInt("Player." + p.getUniqueId() + ".Level") < config.getInt("Armor.Leggings.Level")){
						    	p.sendMessage("§b§lAzarath §8§l» §cDu skal være level " + config.getInt("Armor.Leggings.Level") + " eller over, for at kunne bruge disse leggings!");
								event.setCancelled(true);
								return;
							}
						playerconfig.set("Player." + p.getUniqueId() + ".LeggingsHP", config.getInt("Armor.Leggings.HP"));
						playerconfig.save(playerfile);
						}
							}
					}
		}
		
		}
	}
	
	@EventHandler
	public static void LeggingsInventoryClick(InventoryClickEvent event) throws IOException{
		Player p = (Player)event.getWhoClicked();
		if(event.getInventory().getTitle().equals("container.crafting")){
			if(event.getClick().isRightClick() && !event.getClick().isShiftClick() || event.getClick().isLeftClick() && !event.getClick().isShiftClick()
					|| !event.getClick().isShiftClick() || !event.getClick().isKeyboardClick()){
			if(event.getCurrentItem().getType() == Material.LEATHER_LEGGINGS || event.getCurrentItem().getType() == Material.CHAINMAIL_LEGGINGS ||
					event.getCurrentItem().getType() == Material.GOLD_LEGGINGS || event.getCurrentItem().getType() == Material.IRON_LEGGINGS || 
					event.getCurrentItem().getType() == Material.DIAMOND_LEGGINGS)
			if(event.getRawSlot() == 7){
				File folder = new File(main.instance.getDataFolder().getPath(), "armors/leggings/");
				for(File names : folder.listFiles()){
				File file = new File(main.instance.getDataFolder().getPath(), "armors/leggings/" + names.getName());
				YamlConfiguration config = YamlConfiguration.loadConfiguration(file);
				File playerfile = new File(main.instance.getDataFolder().getPath(), "spillere/" + p.getUniqueId() + ".yml");
				YamlConfiguration playerconfig = YamlConfiguration.loadConfiguration(playerfile);
				if(event.getCurrentItem().getItemMeta().getDisplayName() == null){
					return;
				}else
					if(event.getCurrentItem().getItemMeta().getDisplayName().equals(config.getString("Armor.Leggings.Name").replace("&", "§"))){
						if(playerconfig.getInt("Player." + p.getUniqueId() + ".LeggingsHP") >= config.getInt("Armor.Leggings.HP")){
						
				playerconfig.set("Player." + p.getUniqueId() + ".LeggingsHP", 0);
				playerconfig.save(playerfile);
					}
					}
			}
			}
			
		} 
			if(event.getClick().isRightClick() && !event.getClick().isShiftClick() || event.getClick().isLeftClick() && !event.getClick().isShiftClick()
					|| !event.getClick().isShiftClick() || !event.getClick().isKeyboardClick()){
				if(event.getClick() == event.getClick().SHIFT_LEFT || event.getClick() == event.getClick().SHIFT_RIGHT || event.getClick() == event.getClick().DOUBLE_CLICK || event.getClick() == event.getClick().MIDDLE){
					event.setCancelled(true);
					return;
				}else{
		if (event.getCursor().getType() == Material.LEATHER_LEGGINGS || event.getCursor().getType() == Material.CHAINMAIL_LEGGINGS || event.getCursor().getType() == Material.GOLD_LEGGINGS ||
				event.getCursor().getType() == Material.IRON_LEGGINGS || event.getCursor().getType() == Material.DIAMOND_LEGGINGS){
			if(event.getRawSlot() == 7){
			File folder = new File(main.instance.getDataFolder().getPath(), "armors/leggings/");
			for(File names : folder.listFiles()){
			File file = new File(main.instance.getDataFolder().getPath(), "armors/leggings/" + names.getName());
			YamlConfiguration config = YamlConfiguration.loadConfiguration(file);
			File playerfile = new File(main.instance.getDataFolder().getPath(), "spillere/" + p.getUniqueId() + ".yml");
			YamlConfiguration playerconfig = YamlConfiguration.loadConfiguration(playerfile);
			if(event.getCursor().getItemMeta().getDisplayName() == null){
				return;
			}else
				if(event.getCursor().getItemMeta().getDisplayName().equals(config.getString("Armor.Leggings.Name").replace("&", "§"))){	
					if(playerconfig.getInt("Player." + p.getUniqueId() + ".Level") < config.getInt("Armor.Leggings.Level")){
				    	p.sendMessage("§b§lAzarath §8§l» §cDu skal være level " + config.getInt("Armor.Leggings.Level") + " eller over, for at kunne bruge disse leggings!");
						event.setCancelled(true);
						return;
					}
			playerconfig.set("Player." + p.getUniqueId() + ".LeggingsHP", config.getInt("Armor.Leggings.HP"));
		playerconfig.save(playerfile);
			
				}
				}
			}
			}
		}
		}
	}
	}
	}
	


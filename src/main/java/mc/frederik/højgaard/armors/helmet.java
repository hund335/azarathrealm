package mc.frederik.højgaard.armors;

import java.io.File;
import java.io.IOException;

import org.bukkit.Material;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;

import mc.frederik.højgaard.main;


public class helmet implements Listener {

	 
	@EventHandler
	public static void HelmetClick(PlayerInteractEvent event) throws IOException{
		Player p = (Player)event.getPlayer();
		if(event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK){

		if(p.getInventory().getItemInMainHand().getType() == Material.LEATHER_HELMET || p.getInventory().getItemInMainHand().getType() == Material.CHAINMAIL_HELMET || p.getInventory().getItemInMainHand().getType() == Material.GOLD_HELMET
				|| p.getInventory().getItemInMainHand().getType() == Material.IRON_HELMET || p.getInventory().getItemInMainHand().getType() == Material.DIAMOND_HELMET){
			
						File folder = new File(main.instance.getDataFolder().getPath(), "armors/helmets/");
						for(File names : folder.listFiles()){
						File file = new File(main.instance.getDataFolder().getPath(), "armors/helmets/" + names.getName());
						YamlConfiguration config = YamlConfiguration.loadConfiguration(file);
						File playerfile = new File(main.instance.getDataFolder().getPath(), "spillere/" + p.getUniqueId() + ".yml");
						YamlConfiguration playerconfig = YamlConfiguration.loadConfiguration(playerfile);
						if(event.getItem().getItemMeta().getDisplayName() == null){
							return;
						}else
							if(event.getItem().getItemMeta().getDisplayName().equals(config.getString("Armor.Helmet.Name").replace("&", "§"))){						
						if(p.getInventory().getHelmet() == null){
							if(playerconfig.getInt("Player." + p.getUniqueId() + ".Level") < config.getInt("Armor.Helmet.Level")){
						    	p.sendMessage("§b§lAzarath §8§l» §cDu skal være level " + config.getInt("Armor.Helmet.Level") + " eller over, for at kunne bruge denne helmet!");
								event.setCancelled(true);
								return;
							}
						playerconfig.set("Player." + p.getUniqueId() + ".HelmetHP", config.getInt("Armor.Helmet.HP"));
                        playerconfig.save(playerfile);
						 
						}
							}
					}
		}
		
		}
	}
	
	@EventHandler
	public static void HelmetInventoryClick(InventoryClickEvent event) throws IOException{
		Player p = (Player)event.getWhoClicked();
		if(event.getInventory().getTitle().equals("container.crafting")){
			if(event.getClick().isRightClick() && !event.getClick().isShiftClick() || event.getClick().isLeftClick() && !event.getClick().isShiftClick()
					|| !event.getClick().isShiftClick() || !event.getClick().isKeyboardClick()){
			if(event.getCurrentItem().getType() == Material.LEATHER_HELMET || event.getCurrentItem().getType() == Material.CHAINMAIL_HELMET ||
					event.getCurrentItem().getType() == Material.GOLD_HELMET || event.getCurrentItem().getType() == Material.IRON_HELMET || 
					event.getCurrentItem().getType() == Material.DIAMOND_HELMET)
			if(event.getRawSlot() == 5){
				File folder = new File(main.instance.getDataFolder().getPath(), "armors/helmets/");
				for(File names : folder.listFiles()){
				File file = new File(main.instance.getDataFolder().getPath(), "armors/helmets/" + names.getName());
				YamlConfiguration config = YamlConfiguration.loadConfiguration(file);
				File playerfile = new File(main.instance.getDataFolder().getPath(), "spillere/" + p.getUniqueId() + ".yml");
				YamlConfiguration playerconfig = YamlConfiguration.loadConfiguration(playerfile);
				if(event.getCurrentItem().getItemMeta().getDisplayName() == null){
					return;
				}else
					if(event.getCurrentItem().getItemMeta().getDisplayName().equals(config.getString("Armor.Helmet.Name").replace("&", "§"))){					
						if(playerconfig.getInt("Player." + p.getUniqueId() + ".HelmetHP") >= config.getInt("Armor.Helmet.HP")){
							playerconfig.set("Player." + p.getUniqueId() + ".HelmetHP", 0);
							playerconfig.save(playerfile);
			
					}
					}
			}
			}
			
		} 
			if(event.getClick().isRightClick() && !event.getClick().isShiftClick() || event.getClick().isLeftClick() && !event.getClick().isShiftClick()
					|| !event.getClick().isShiftClick() || !event.getClick().isKeyboardClick()){
				if(event.getClick() == event.getClick().SHIFT_LEFT || event.getClick() == event.getClick().SHIFT_RIGHT || event.getClick() == event.getClick().DOUBLE_CLICK || event.getClick() == event.getClick().MIDDLE){
					event.setCancelled(true);
					return;
				}else{
		if (event.getCursor().getType() == Material.LEATHER_HELMET || event.getCursor().getType() == Material.CHAINMAIL_HELMET || event.getCursor().getType() == Material.GOLD_HELMET ||
				event.getCursor().getType() == Material.IRON_HELMET || event.getCursor().getType() == Material.DIAMOND_HELMET){
			if(event.getRawSlot() == 5){
			File folder = new File(main.instance.getDataFolder().getPath(), "armors/helmets/");
			for(File names : folder.listFiles()){
			File file = new File(main.instance.getDataFolder().getPath(), "armors/helmets/" + names.getName());
			YamlConfiguration config = YamlConfiguration.loadConfiguration(file);
			File playerfile = new File(main.instance.getDataFolder().getPath(), "spillere/" + p.getUniqueId() + ".yml");
			YamlConfiguration playerconfig = YamlConfiguration.loadConfiguration(playerfile);
			if(event.getCursor().getItemMeta().getDisplayName() == null){
				return;
			}else
				if(event.getCursor().getItemMeta().getDisplayName().equals(config.getString("Armor.Helmet.Name").replace("&", "§"))){	
					if(playerconfig.getInt("Player." + p.getUniqueId() + ".Level") < config.getInt("Armor.Helmet.Level")){
				    	p.sendMessage("§b§lAzarath §8§l» §cDu skal være level " + config.getInt("Armor.Helmet.Level") + " eller over, for at kunne bruge denne helmet!");
						event.setCancelled(true);
						return;
					}
			playerconfig.set("Player." + p.getUniqueId() + ".HelmetHP", config.getInt("Armor.Helmet.HP"));
            playerconfig.save(playerfile);
			
				}
				}
			}
			}
		}
		}
	}
	}
	}
	


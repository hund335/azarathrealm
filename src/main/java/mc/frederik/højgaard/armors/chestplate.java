package mc.frederik.højgaard.armors;

import java.io.File;
import java.io.IOException;

import org.bukkit.Material;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;

import mc.frederik.højgaard.main;


public class chestplate implements Listener {
	
	@EventHandler
	public static void ChestplateClick(PlayerInteractEvent event) throws IOException{
		Player p = (Player)event.getPlayer();
		if(event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK){

		if(p.getInventory().getItemInMainHand().getType() == Material.LEATHER_CHESTPLATE || p.getInventory().getItemInMainHand().getType() == Material.CHAINMAIL_CHESTPLATE || p.getInventory().getItemInMainHand().getType() == Material.GOLD_CHESTPLATE
				|| p.getInventory().getItemInMainHand().getType() == Material.IRON_CHESTPLATE || p.getInventory().getItemInMainHand().getType() == Material.DIAMOND_CHESTPLATE){
			
						File folder = new File(main.instance.getDataFolder().getPath(), "armors/chestplates/");
						for(File names : folder.listFiles()){
						File file = new File(main.instance.getDataFolder().getPath(), "armors/chestplates/" + names.getName());
						YamlConfiguration config = YamlConfiguration.loadConfiguration(file);
						File playerfile = new File(main.instance.getDataFolder().getPath(), "spillere/" + p.getUniqueId() + ".yml");
						YamlConfiguration playerconfig = YamlConfiguration.loadConfiguration(playerfile);
						if(event.getItem().getItemMeta().getDisplayName() == null){
							return;
						}else
							if(event.getItem().getItemMeta().getDisplayName().equals(config.getString("Armor.Chestplate.Name").replace("&", "§"))){						
						if(p.getInventory().getChestplate() == null){
							if(playerconfig.getInt("Player." + p.getUniqueId() + ".Level") < config.getInt("Armor.Chestplate.Level")){
						    	p.sendMessage("§b§lAzarath §8§l» §cDu skal være level " + config.getInt("Armor.Chestplate.Level") + " eller over, for at kunne bruge denne chestplate!");
								event.setCancelled(true);
								return;
							}
						playerconfig.set("Player." + p.getUniqueId() + ".ChestplateHP", config.getInt("Armor.Chestplate.HP"));
						playerconfig.save(playerfile);
						}
							}
					}
		}
		
		}
	}
	
	@EventHandler
	public static void ChestplateInventoryClick(InventoryClickEvent event) throws IOException{
		Player p = (Player)event.getWhoClicked();
		if(event.getInventory().getTitle().equals("container.crafting")){
			if(event.getClick().isRightClick() && !event.getClick().isShiftClick() || event.getClick().isLeftClick() && !event.getClick().isShiftClick()
					|| !event.getClick().isShiftClick() || !event.getClick().isKeyboardClick()){
			if(event.getCurrentItem().getType() == Material.LEATHER_CHESTPLATE || event.getCurrentItem().getType() == Material.CHAINMAIL_CHESTPLATE ||
					event.getCurrentItem().getType() == Material.GOLD_CHESTPLATE || event.getCurrentItem().getType() == Material.IRON_CHESTPLATE || 
					event.getCurrentItem().getType() == Material.DIAMOND_CHESTPLATE)
			if(event.getRawSlot() == 6){
				File folder = new File(main.instance.getDataFolder().getPath(), "armors/chestplates/");
				for(File names : folder.listFiles()){
				File file = new File(main.instance.getDataFolder().getPath(), "armors/chestplates/" + names.getName());
				YamlConfiguration config = YamlConfiguration.loadConfiguration(file);
				File playerfile = new File(main.instance.getDataFolder().getPath(), "spillere/" + p.getUniqueId() + ".yml");
				YamlConfiguration playerconfig = YamlConfiguration.loadConfiguration(playerfile);
				if(event.getCurrentItem().getItemMeta().getDisplayName() == null){
					return;
				}else
					if(event.getCurrentItem().getItemMeta().getDisplayName().equals(config.getString("Armor.Chestplate.Name").replace("&", "§"))){
						if(playerconfig.getInt("Player." + p.getUniqueId() + ".ChestplateHP") >= config.getInt("Armor.Chestplate.HP")){
							playerconfig.set("Player." + p.getUniqueId() + ".ChestplateHP", 0);
						    playerconfig.save(playerfile);
			
					}
					}
			}
			}
			
		} 
			if(event.getClick().isRightClick() && !event.getClick().isShiftClick() || event.getClick().isLeftClick() && !event.getClick().isShiftClick()
					|| !event.getClick().isShiftClick() || !event.getClick().isKeyboardClick()){
				if(event.getClick() == event.getClick().SHIFT_LEFT || event.getClick() == event.getClick().SHIFT_RIGHT || event.getClick() == event.getClick().DOUBLE_CLICK || event.getClick() == event.getClick().MIDDLE){
					event.setCancelled(true);
					return;
				}else{
		if (event.getCursor().getType() == Material.LEATHER_CHESTPLATE || event.getCursor().getType() == Material.CHAINMAIL_CHESTPLATE || event.getCursor().getType() == Material.GOLD_CHESTPLATE ||
				event.getCursor().getType() == Material.IRON_CHESTPLATE || event.getCursor().getType() == Material.DIAMOND_CHESTPLATE){
			if(event.getRawSlot() == 6){
			File folder = new File(main.instance.getDataFolder().getPath(), "armors/chestplates/");
			for(File names : folder.listFiles()){
			File file = new File(main.instance.getDataFolder().getPath(), "armors/chestplates/" + names.getName());
			YamlConfiguration config = YamlConfiguration.loadConfiguration(file);
			File playerfile = new File(main.instance.getDataFolder().getPath(), "spillere/" + p.getUniqueId() + ".yml");
			YamlConfiguration playerconfig = YamlConfiguration.loadConfiguration(playerfile);
			if(event.getCursor().getItemMeta().getDisplayName() == null){
				return;
			}else
				if(event.getCursor().getItemMeta().getDisplayName().equals(config.getString("Armor.Chestplate.Name").replace("&", "§"))){
					if(playerconfig.getInt("Player." + p.getUniqueId() + ".Level") < config.getInt("Armor.Chestplate.Level")){
				    	p.sendMessage("§b§lAzarath §8§l» §cDu skal være level " + config.getInt("Armor.Chestplate.Level") + " eller over, for at kunne bruge denne chestplate!");
						event.setCancelled(true);
						return;
					}
			playerconfig.set("Player." + p.getUniqueId() + ".ChestplateHP", config.getInt("Armor.Chestplate.HP"));
		    playerconfig.save(playerfile);
			
				}
				}
			}
			}
		}
		}
	}
	}
	}
	


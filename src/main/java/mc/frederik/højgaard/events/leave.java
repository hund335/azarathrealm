package mc.frederik.højgaard.events;

import java.util.Calendar;
import java.util.TimeZone;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerQuitEvent;

import mc.frederik.højgaard.api.mysql;

public class leave {

	
	@EventHandler
	  public static void Leave(PlayerQuitEvent event){
		 mysql ms = new mysql();
		 Player p = event.getPlayer();
		event.setQuitMessage(null);
		Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT+1"));
		
		   int year = cal.get(Calendar.YEAR);
		   int month = cal.get(Calendar.MONTH) + 1;
		   int day = cal.get(Calendar.DAY_OF_MONTH);
		   int hours = cal.get(Calendar.HOUR) + 1;
		   int minutes = cal.get(Calendar.MINUTE);
		   int seconds = cal.get(Calendar.SECOND);
		
		ms.Update(p, "LastOnline", String.valueOf(day + "/" + month + "/" + year + " - " + hours + ":" + minutes + ":" + seconds));
	}
	
}

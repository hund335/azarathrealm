package mc.frederik.højgaard.events;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.scheduler.BukkitRunnable;

import com.gmail.filoghost.holographicdisplays.api.Hologram;
import com.gmail.filoghost.holographicdisplays.api.HologramsAPI;

import mc.frederik.højgaard.main;


public class yourdamage {

	@SuppressWarnings("deprecation")
	@EventHandler
	public static void PlayerDamageMeal(EntityDamageByEntityEvent event){		
		if (event.getDamager() instanceof Player){
			if(event.isCancelled() == false){
			  Player p = (Player)event.getDamager();
			  
			
			  Entity d = (Entity)event.getEntity();
			  if(!main.horsedata.containsValue(d.getUniqueId())){
				  
			  Location loc = d.getLocation();
			  int y = loc.getBlockY() + 2;
			  loc.setY(y);
			  
		        Hologram holo2 =  HologramsAPI.createHologram(main.instance, loc);
		        holo2.appendTextLine("§c- " + event.getDamage() + " ♥");
		        holo2.getVisibilityManager().showTo(p);
		        holo2.getVisibilityManager().setVisibleByDefault(false);
		   
			 Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(main.instance, new BukkitRunnable()
			    {
			      public void run()
			      {
			    	holo2.delete();
			        
			      }
			    
			    }, 20L);
		}
		}
		}
	}
	@EventHandler
	public static void PlayerDamageShoot(EntityDamageByEntityEvent event){
		Projectile damager = (Projectile) event.getDamager();
	    if(damager.getShooter() instanceof Player){
		
			Entity shooter = (Entity) damager.getShooter();
			Player d = (Player)shooter;
			
			  if(!main.horsedata.containsValue(event.getEntity().getUniqueId())){
			  Location loc = event.getEntity().getLocation();
			  int y = loc.getBlockY() + 2;
			  loc.setY(y);
			  
		        Hologram holo2 =  HologramsAPI.createHologram(main.instance, loc);
		        holo2.appendTextLine("§c- " + event.getDamage() + " ♥");
		        holo2.getVisibilityManager().showTo(d);
		        holo2.getVisibilityManager().setVisibleByDefault(false);
		   
			 Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(main.instance, new BukkitRunnable()
			    {
			      public void run()
			      {
			    	holo2.delete();
			        
			      }
			    
			    }, 20L);
		}
	}
}
}
		

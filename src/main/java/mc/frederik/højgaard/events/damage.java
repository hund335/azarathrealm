package mc.frederik.højgaard.events;

import java.io.File;
import java.util.HashMap;
import java.util.Random;

import org.bukkit.EntityEffect;
import org.bukkit.Sound;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Skeleton;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import mc.frederik.højgaard.main;
import mc.frederik.højgaard.api.api;


public class damage implements Listener{

	  public static HashMap<String, Long> cooldowns = new HashMap();
	
	  
	  
	@EventHandler
	public static void HP(EntityDamageEvent event) {
		
		if (event.getEntity() instanceof Player){
		Player p = (Player)event.getEntity();
		api api = new api();
        if(event.getCause() != DamageCause.FALL){
	      int cooldownTime1 = 2;
	      if (cooldowns.containsKey(p.getName()))
	      {
	        long secondsLeft = ((Long)cooldowns.get(p.getName())).longValue() / 1000L + cooldownTime1 - System.currentTimeMillis() / 1000L;
	        if (secondsLeft > 0L)
	        {
	          event.setCancelled(true);
	          return;
	        }
	      }
	      cooldowns.put(p.getName(), Long.valueOf(System.currentTimeMillis()));
	    
	      if(api.getHP(p) > 0){
	    		p.playSound(p.getLocation(), Sound.ENTITY_PLAYER_HURT, 2, 3);
				//p.setVelocity(p.getLocation().getDirection().multiply(-1));
				p.playEffect(EntityEffect.HURT);
				int damage = api.getHP(p) - (int)event.getDamage();
	            api.UpdateHP(p, damage);
	  
	    
	    

	    	
	        if(api.getHP(p) <= 0){	     
	        	return;	        	
	        } 
	    
	    	event.setCancelled(true);
	        }
	    } 
	}
	}	
	
}
	
	    


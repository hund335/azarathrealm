package mc.frederik.højgaard.events;

import java.io.IOException;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityCombustEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.entity.ExplosionPrimeEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemConsumeEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.event.player.PlayerSwapHandItemsEvent;

import mc.frederik.højgaard.ai.javis;
import mc.frederik.højgaard.armors.boots;
import mc.frederik.højgaard.armors.chestplate;
import mc.frederik.højgaard.armors.helmet;
import mc.frederik.højgaard.armors.leggings;
import mc.frederik.højgaard.horse.horse;
import mc.frederik.højgaard.horse.horseclick;
import mc.frederik.højgaard.horse.horsedamage;
import mc.frederik.højgaard.horse.horseinventory;
import mc.frederik.højgaard.horse.horseremove;
import mc.frederik.højgaard.mobs.zombie;
import mc.frederik.højgaard.potions.potion;
import mc.frederik.højgaard.weapons.bow;
import mc.frederik.højgaard.weapons.iron_sword;
import mc.frederik.højgaard.weapons.wand;

public class register implements Listener {

	@EventHandler
	  public void JoinRegister(PlayerJoinEvent event){
	 join.FirstJoin(event);
		
	  }
	
	
	@EventHandler
	public void onExpload(ExplosionPrimeEvent event){
		expload.onExpload(event);
	}
	
	@EventHandler
	public void PlaceRegister(BlockPlaceEvent event){
		place.onPlace(event);
	}
	
	@EventHandler
	  public void LeaveRegister(PlayerQuitEvent event){
		leave.Leave(event);
		horseremove.RemoveHorse(event);
	}
	@EventHandler
	public void ChatRegister(AsyncPlayerChatEvent event){
		chat.Prefix(event);
		javis.Chat(event);
	}
	@EventHandler
	public void MobDamageRegister(EntityDamageByEntityEvent event) {
	zombie.Damage(event);
	iron_sword.SwordDamage(event);
	yourdamage.PlayerDamageMeal(event);
	yourdamage.PlayerDamageShoot(event);
	//damage.HP(event);
		
	}
	 @EventHandler
	  public void PotionEvent(PlayerItemConsumeEvent event){
		 potion.PotionEvent(event);
	 }
	
	@EventHandler
	public void EntityDamageRegister(EntityDamageEvent event){
		falldamage.FallDamage(event);
		horsedamage.Damage(event);
	}
	
	@EventHandler
	public void RespawnRegister(PlayerRespawnEvent event){
		respawn.Respawn(event);
		}
	@EventHandler
	public void SecondhandRegister(PlayerSwapHandItemsEvent event){
		secondhand.Secondhand(event);
	}
	@EventHandler
	public void FireDamageRegister(EntityCombustEvent e){
	firedamage.FireDamage(e);
	
	}
	@EventHandler
	public void DeathRegister(EntityDeathEvent event){
		zombie.DeathMessage(event);
		
	}
	@EventHandler
	public void StopBowsRegister(EntityShootBowEvent event){
		if (event.getEntity() instanceof Player){
			event.setCancelled(true);
		}
	}
	@EventHandler
    public static void ProjectileHitRegister(ProjectileHitEvent event){
		wand.onland(event);
 
    }
	@EventHandler
	public void PlayerInteractRegister(PlayerInteractEvent event) throws IOException{
	horse.HorseItem(event);
	wand.WandSooting(event);
	bow.BowSooting(event);
	helmet.HelmetClick(event);
	chestplate.ChestplateClick(event);
	leggings.LeggingsClick(event);
	boots.BootsClick(event);
	
}
	
	@EventHandler
	public void PlayerInteractEntityRegister(PlayerInteractEntityEvent event){
		horseclick.HorseClicking(event);
	}
	
	@EventHandler
	public static void InventoryClickRegister(InventoryClickEvent event) throws IOException{
		horseinventory.HorseInv(event);
		register2.DisableClicks(event);
		helmet.HelmetInventoryClick(event);
		chestplate.ChestplateInventoryClick(event);
		leggings.LeggingsInventoryClick(event);
		boots.BootsInventoryClick(event);
		
	}
	  @EventHandler
	  public void PlayerDeathRegister(PlayerDeathEvent event) throws IOException{
	register2.Death(event);
	  }
	}
	


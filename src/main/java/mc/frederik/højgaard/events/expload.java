package mc.frederik.højgaard.events;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.ExplosionPrimeEvent;

public class expload implements Listener{
	
	@EventHandler
	public static void onExpload(ExplosionPrimeEvent event){
		
		event.setRadius(0.0F);
		event.setFire(false);
		event.setCancelled(true);
	}

}

package mc.frederik.højgaard.admincmds;


import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import mc.frederik.højgaard.main;


public class givearmor
  implements CommandExecutor, Listener
{

	public static String arg1;
	public static String arg2;
  
  
  public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args)
  {
    if (!(sender instanceof Player))
    {
      sender.sendMessage("§b§lAzarath §8§l» §cFejl: du kan ikke bruge denne kommando over konsolen!");
    
      return true;
    }
    Player p = (Player)sender;
    if (commandLabel.equalsIgnoreCase("givearmor")) {
      if (p.hasPermission("givearmor.use"))
      {
        if (args.length <= 1)
        {
        	    p.sendMessage("§b§lAzarath §8§l» §cFejl: brug /givearmor <model> <navn>");
        	
        	/*    
        	    ItemStack is = new ItemStack(Material.MOB_SPAWNER);
        	    net.minecraft.server.v1_11_R1.ItemStack CIS = CraftItemStack.asNMSCopy(is);
        	    NBTTagCompound tag = new NBTTagCompound();
        	    NBTTagCompound subtag = new NBTTagCompound();
        	    //tag.set("BlockEntityTag", new NBTTagString("SpawnData:{id:Skeleton}"));
        	    //subtag.set("SpawnData", new NBTTagString("Skeleton"));
        	   // subtag.set("SpawnData", tag.getCompound("BlockEntityTag").getCompound("SpawnData"));
        	    subtag.set("EntityId", new NBTTagString("Zombie"));

        	    //subtag.set(tag.getCompound("BlockEntityTag").getCompound("SpawnData"), new NBTTagString(""));
        	    tag.set("BlockEntityTag", subtag);
        	    //subtag.set("SpawnData", new NBTTagString("Skeleton"));
        	   
        	    CIS.setTag(tag);
        	    p.sendMessage("Tags: " + CIS.getTag());
        	   ItemStack spawndata = CraftItemStack.asBukkitCopy(CIS);
        	   p.getInventory().addItem(spawndata);
        	   p.updateInventory();
        	   */
        	return false;
        }
        	 if(args.length >= 2)
        	      arg1 = "";
        	      arg1 += args[0];
        	      
        	      arg2 = "";
        	      arg2 += args[1];
        	      
    
        	      
        	  
        	      
        	      if(arg1.equalsIgnoreCase("helmet")){
            	      File file = new File(main.instance.getDataFolder().getPath(), "armors/helmets/" + arg2 + ".yml");
          		    YamlConfiguration config = YamlConfiguration.loadConfiguration(file);
          		    
        	    	  if(file.exists()){
        	    		  ItemStack armor = new ItemStack(Material.getMaterial(config.getString("Armor.Helmet.Type").replace("LEATHER", "LEATHER_HELMET").replace("CHAIN", "CHAINMAIL_HELMET")
        	    				  .replace("GOLD", "GOLD_HELMET").replace("IRON", "IRON_HELMET").replace("DIAMOND", "DIAMOND_HELMET")));
         	    		 ItemMeta armormeta = armor.getItemMeta();
         	    		 armormeta.spigot().setUnbreakable(true);
         	    		 armormeta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
         	    		 armormeta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
         	    		armormeta.setDisplayName(config.getString("Armor.Helmet.Name").replace("&", "§"));
         	    	      if (config.getBoolean("Armor.Helmet.Lore") == true)
         	    	      {
         	    	        List<String> lore = new ArrayList();
         	    	        List<String> loredata = config.getStringList("Armor.Helmet.Lores");
         	    	        for (int i = 0; i < loredata.size(); i++)
         	    	        {
         	    	          lore.add(loredata.get(i).replace("&", "§").replace("%HP%", "" + config.getInt("Armor.Helmet.HP")).replace("%Level%", "" + config.getInt("Armor.Helmet.Level")));
         	    	          armormeta.setLore(lore);
         	    	        }
         	    	      }
         	    		armor.setItemMeta(armormeta);
         	    		 p.getInventory().addItem(armor);
         	    		 p.updateInventory();
      
        	    		  return false;
        	    	  }else{
        	    		  p.sendMessage("§b§lAzarath §8§l» §cFejl: denne helmet ser ikke ud til at findes!"); 
        	    	  }
        	    	  
        	      } else   if(arg1.equalsIgnoreCase("chestplate")){
            	      File file = new File(main.instance.getDataFolder().getPath(), "armors/chestplates/" + arg2 + ".yml");
          		    YamlConfiguration config = YamlConfiguration.loadConfiguration(file);
        	    	  if(file.exists()){
        	    		  ItemStack armor = new ItemStack(Material.getMaterial(config.getString("Armor.Chestplate.Type").replace("LEATHER", "LEATHER_CHESTPLATE").replace("CHAIN", "CHAINMAIL_CHESTPLATE")
        	    				  .replace("GOLD", "GOLD_CHESTPLATE").replace("IRON", "IRON_CHESTPLATE").replace("DIAMOND", "DIAMOND_CHESTPLATE")));
         	    		 ItemMeta armormeta = armor.getItemMeta();
         	    		 armormeta.spigot().setUnbreakable(true);
         	    		 armormeta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
         	    		 armormeta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
         	    		armormeta.setDisplayName(config.getString("Armor.Chestplate.Name").replace("&", "§"));
         	    	      if (config.getBoolean("Armor.Chestplate.Lore") == true)
         	    	      {
         	    	        List<String> lore = new ArrayList();
         	    	        List<String> loredata = config.getStringList("Armor.Chestplate.Lores");
         	    	        for (int i = 0; i < loredata.size(); i++)
         	    	        {
         	    	          lore.add(loredata.get(i).replace("&", "§").replace("%HP%", "" + config.getInt("Armor.Chestplate.HP")).replace("%Level%", "" + config.getInt("Armor.Chestplate.Level")));
         	    	          armormeta.setLore(lore);
         	    	        }
         	    	      }
         	    		armor.setItemMeta(armormeta);
         	    		 p.getInventory().addItem(armor);
         	    		 p.updateInventory();
      
        	    	      
        	    		  return false;
        	    	  }else{
        	    		  p.sendMessage("§b§lAzarath §8§l» §cFejl: denne chestplate ser ikke ud til at findes!"); 
        	    	  }
        	      }
        	    	  else  if(arg1.equalsIgnoreCase("leggings")){
        	    	      File file = new File(main.instance.getDataFolder().getPath(), "armors/leggings/" + arg2 + ".yml");
              		    YamlConfiguration config = YamlConfiguration.loadConfiguration(file);
            	    	  if(file.exists()){
            	    		  ItemStack armor = new ItemStack(Material.getMaterial(config.getString("Armor.Leggings.Type").replace("LEATHER", "LEATHER_LEGGINGS").replace("CHAIN", "CHAINMAIL_LEGGINGS")
            	    				  .replace("GOLD", "GOLD_LEGGINGS").replace("IRON", "IRON_LEGGINGS").replace("DIAMOND", "DIAMOND_LEGGINGS")));
             	    		 ItemMeta armormeta = armor.getItemMeta();
             	    		 armormeta.spigot().setUnbreakable(true);
             	    		 armormeta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
             	    		 armormeta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
             	    		armormeta.setDisplayName(config.getString("Armor.Leggings.Name").replace("&", "§"));
             	    	      if (config.getBoolean("Armor.Leggings.Lore") == true)
             	    	      {
             	    	        List<String> lore = new ArrayList();
             	    	        List<String> loredata = config.getStringList("Armor.Leggings.Lores");
             	    	        for (int i = 0; i < loredata.size(); i++)
             	    	        {
             	    	          lore.add(loredata.get(i).replace("&", "§").replace("%HP%", "" + config.getInt("Armor.Leggings.HP")).replace("%Level%", "" + config.getInt("Armor.Leggings.Level")));
             	    	          armormeta.setLore(lore);
             	    	        }
             	    	      }
             	    		armor.setItemMeta(armormeta);
             	    		 p.getInventory().addItem(armor);
             	    		 p.updateInventory();
          
            	    	      
            	    		  return false;
            	    	  }else{
            	    		  p.sendMessage("§b§lAzarath §8§l» §cFejl: disse leggings ser ikke ud til at findes!"); 
            	    	  }
        	    	  }
        	    	  else   if(arg1.equalsIgnoreCase("boots")){
        	    	      File file = new File(main.instance.getDataFolder().getPath(), "armors/boots/" + arg2 + ".yml");
              		    YamlConfiguration config = YamlConfiguration.loadConfiguration(file);
                	    	  if(file.exists()){
                	    		  ItemStack armor = new ItemStack(Material.getMaterial(config.getString("Armor.Boots.Type").replace("LEATHER", "LEATHER_BOOTS").replace("CHAIN", "CHAINMAIL_BOOTS")
                	    				  .replace("GOLD", "GOLD_BOOTS").replace("IRON", "IRON_BOOTS").replace("DIAMOND", "DIAMOND_BOOTS")));
                 	    		 ItemMeta armormeta = armor.getItemMeta();
                 	    		 armormeta.spigot().setUnbreakable(true);
                 	    		 armormeta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
                 	    		 armormeta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
                 	    		armormeta.setDisplayName(config.getString("Armor.Boots.Name").replace("&", "§"));
                 	    	      if (config.getBoolean("Armor.Boots.Lore") == true)
                 	    	      {
                 	    	        List<String> lore = new ArrayList();
                 	    	        List<String> loredata = config.getStringList("Armor.Boots.Lores");
                 	    	        for (int i = 0; i < loredata.size(); i++)
                 	    	        {
                 	    	          lore.add(loredata.get(i).replace("&", "§").replace("%HP%", "" + config.getInt("Armor.Boots.HP")).replace("%Level%", "" + config.getInt("Armor.Boots.Level")));
                 	    	          armormeta.setLore(lore);
                 	    	        }
                 	    	      }
                 	    		armor.setItemMeta(armormeta);
                 	    		 p.getInventory().addItem(armor);
                 	    		 p.updateInventory();
              
                	    	      
                	    		  return false;
                	    	  }else{
                	    		  p.sendMessage("§b§lAzarath §8§l» §cFejl: disse boots ser ikke ud til at findes!"); 
                	    	  }
        	      } else{
        	    	  p.sendMessage("§b§lAzarath §8§l» §cFejl: denne armor type ser ikke ud til at findes!"); 
        	    	  return false;
        	      }
        	      
        	      
        }
      }
    
	return false;
  }
}

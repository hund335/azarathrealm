package mc.frederik.højgaard.admincmds;


import java.io.File;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Zombie;
import org.bukkit.event.Listener;

import mc.frederik.højgaard.main;
import mc.frederik.højgaard.mobs.zombie;


public class spawnmob
  implements CommandExecutor, Listener
{

	public static String arg1;
	public static String arg2;
  
  
  public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args)
  {
    if (!(sender instanceof Player))
    {
      sender.sendMessage("§b§lAzarath §8§l» §cFejl: du kan ikke bruge denne kommando over konsolen!");
      return true;
    }
    Player p = (Player)sender;
    if (commandLabel.equalsIgnoreCase("spawnmob")) {
      if (p.hasPermission("spawnmob.use"))
      {
        if (args.length <= 1)
        {
        	    p.sendMessage("§b§lAzarath §8§l» §cFejl: brug /spawnmob <model> <id>");
        	return false;
        }
        	 if(args.length >= 2)
        	      arg1 = "";
        	      arg1 += args[0];
        	      
        	      arg2 = "";
        	      arg2 += args[1];
        	      
        	      File file = new File(main.instance.getDataFolder().getPath(), "mobs/zombies/" + arg2 + ".yml");
        		    YamlConfiguration config = YamlConfiguration.loadConfiguration(file);
        	      
        	      if(arg1.equalsIgnoreCase("zombie")){
        	    	  
     	    		 zombie.create((Zombie)p.getWorld().spawnEntity(p.getLocation(), EntityType.ZOMBIE), arg2, p);

        	      } else if(arg1.equalsIgnoreCase("skeleton")){
        	    	  
        	      } else{
        	    	  p.sendMessage("§b§lAzarath §8§l» §cFejl: denne model ser ikke ud til at findes!"); 
        	    	  return false;
        	      }
        	      
        	      
        }
      }
    
	return false;
  }
}

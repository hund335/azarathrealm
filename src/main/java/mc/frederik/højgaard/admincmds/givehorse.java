package mc.frederik.højgaard.admincmds;


import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class givehorse
  implements CommandExecutor, Listener
{


  
  
  public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args)
  {
    if (!(sender instanceof Player))
    {
      sender.sendMessage("§b§lAzarath §8§l» §cFejl: du kan ikke bruge denne kommando over konsolen!");
      return true;
    }
    Player p = (Player)sender;
    Inventory i = p.getInventory();
    if (commandLabel.equalsIgnoreCase("givehorse")) {
      if (p.hasPermission("givehorse.use"))
      {
        if (args.length == 0)
        {
        	    p.sendMessage("§b§lAzarath §8§l» §cFejl: brug /givehorse <level>");
        	return false;
        }
        	 if(args.length >= 1){
        	     int arg1;
        	      arg1 = Integer.valueOf(args[0]);
        	      
        	      if(arg1 == 1){
        	    	  ItemStack is = new ItemStack(Material.SADDLE);
        	    	  ItemMeta im = is.getItemMeta();
        	    	  im.setDisplayName("§aHest §8(§7Tier 1§8)");
        	    	  is.setItemMeta(im);
        	    	  i.addItem(is);
        	    	  p.updateInventory();
        	      } else if(arg1 == 2){
        	    	  ItemStack is = new ItemStack(Material.SADDLE);
        	    	  ItemMeta im = is.getItemMeta();
        	    	  im.setDisplayName("§aHest §8(§7Tier 2§8)");
        	    	  is.setItemMeta(im);
        	    	  i.addItem(is);
        	    	  p.updateInventory();
        	    	  
        	      } else if(arg1 == 3){
        	    	  ItemStack is = new ItemStack(Material.SADDLE);
        	    	  ItemMeta im = is.getItemMeta();
        	    	  im.setDisplayName("§aHest §8(§7Tier 3§8)");
        	    	  is.setItemMeta(im);
        	    	  i.addItem(is);
        	    	  p.updateInventory();
        	    	  
        	      } else if(arg1 == 4){
        	    	  ItemStack is = new ItemStack(Material.SADDLE);
        	    	  ItemMeta im = is.getItemMeta();
        	    	  im.setDisplayName("§aHest §8(§7Tier 4§8)");
        	    	  is.setItemMeta(im);
        	    	  i.addItem(is);
        	    	  p.updateInventory();
        	    	  
        	      }else{
        	    	  p.sendMessage("§b§lAzarath §8§l» §cFejl: denne findes kun level 1-4 heste!");
        	      }
      }
      }
    }
	return false;
    }
        	      }
package mc.frederik.højgaard.admincmds;


import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import mc.frederik.højgaard.main;
import mc.frederik.højgaard.weapons.bow;
import mc.frederik.højgaard.weapons.wand;


public class giveweapon
  implements CommandExecutor, Listener
{

	public static String arg1;
	public static String arg2;
	public bow bows = new bow();
	public wand wands = new wand();
  
  
  public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args)
  {
    if (!(sender instanceof Player))
    {
      sender.sendMessage("§b§lAzarath §8§l» §cFejl: du kan ikke bruge denne kommando over konsolen!");
      return true;
    }
    Player p = (Player)sender;
    if (commandLabel.equalsIgnoreCase("giveweapon")) {
      if (p.hasPermission("giveweapon.use"))
      {
        if (args.length <= 1)
        {
        	    p.sendMessage("§b§lAzarath §8§l» §cFejl: brug /giveweapon <type> <id>");
        	return false;
        }
        	 if(args.length >= 2)
        	      arg1 = "";
        	      arg1 += args[0];
        	      
        	      arg2 = "";
        	      arg2 += args[1];
        	      
        	     
        	      
        	      //p.sendMessage("Arg1: " + arg1 + " | Arg2: " + arg2);
        	      
        	      if(arg1.equalsIgnoreCase("wand")){
                      wands.getWand(p, arg2);
        	    		  
        	    	  
        	    	  
        	      } else if(arg1.equalsIgnoreCase("bow")){
        	    	 bows.getBow(p, arg2);
     	
        	      } else if(arg1.equalsIgnoreCase("iron_sword")){ 
        	    	  File file = new File(main.instance.getDataFolder().getPath(), "weapons/iron_swords/" + arg2 + ".yml");
          		    YamlConfiguration config = YamlConfiguration.loadConfiguration(file);
          		    
        	    	  if(file.exists()){
        	    		ItemStack isword = new ItemStack(Material.IRON_SWORD);
        	    		 ItemMeta imeta = isword.getItemMeta();
        	    		imeta.setDisplayName(config.getString("Weapon.Iron_Sword.Name").replace("&", "§"));
        	    	      if (config.getBoolean("Weapon.Iron_Sword.Lore") == true)
        	    	      {
        	    	        List<String> lore = new ArrayList();
        	    	        List<String> loredata = config.getStringList("Weapon.Iron_Sword.Lores");
        	    	        for (int i = 0; i < loredata.size(); i++)
        	    	        {
        	    	          lore.add(loredata.get(i).replace("&", "§").replace("%MinDamage%", "" + config.getInt("Weapon.Iron_Sword.MinDamage")).replace("%MaxDamage%", "" + config.getInt("Weapon.Iron_Sword.MaxDamage")).replace("%Level%", "" + config.getInt("Weapon.Iron_Sword.Level")));
        	    	          imeta.setLore(lore);
        	    	        }
        	    	      }
        	    	      imeta.spigot().setUnbreakable(true);
        	    	      imeta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
        	    	      imeta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        	    		isword.setItemMeta(imeta);
        	    		 p.getInventory().addItem(isword);
        	    		 p.updateInventory();
        	    		  return false;
        	    	  }else{
        	    		  p.sendMessage("§b§lAzarath §8§l» §cFejl: dette iron sword ser ikke ud til at findes!"); 
        	    	  }
        	      
        	      
        	      }else{
        	    	  p.sendMessage("§b§lAzarath §8§l» §cFejl: denne weapon type ser ikke ud til at findes!"); 
        	    	  return false;
        	      }        
      }
    }
	return false;
  }
}

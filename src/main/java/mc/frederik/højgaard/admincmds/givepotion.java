package mc.frederik.højgaard.admincmds;


import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import mc.frederik.højgaard.main;


public class givepotion
  implements CommandExecutor, Listener
{

	public static String arg1;
  
  
  public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args)
  {
    if (!(sender instanceof Player))
    {
      sender.sendMessage("§b§lAzarath §8§l» §cFejl: du kan ikke bruge denne kommando over konsolen!");
      return true;
    }
    Player p = (Player)sender;
    if (commandLabel.equalsIgnoreCase("givepotion")) {
      if (p.hasPermission("givepotion.use"))
      {
        if (args.length < 1)
        {
        	    p.sendMessage("§b§lAzarath §8§l» §cFejl: brug /givepotion <navn>");
        	return false;
        }
        	 if(args.length >= 1)
        	      arg1 = "";
        	      arg1 += args[0];
        	      
        	  
        	    	  File file = new File(main.instance.getDataFolder().getPath(), "potions/" + arg1 + ".yml");
          		    YamlConfiguration config = YamlConfiguration.loadConfiguration(file);
          		    
        	    	  if(file.exists()){
        	    		ItemStack potion = new ItemStack(Material.POTION,1,(short)8197);
        	    		 ItemMeta potionmeta = potion.getItemMeta();
        	    		potionmeta.setDisplayName(config.getString("Potion.Name").replace("&", "§"));
        	    		potionmeta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
        	    	      if (config.getBoolean("Potion.Lore") == true)
        	    	      {
        	    	        List<String> lore = new ArrayList();
        	    	        List<String> loredata = config.getStringList("Potion.Lores");
        	    	        for (int i = 0; i < loredata.size(); i++)
        	    	        {
        	    	          lore.add(loredata.get(i).replace("&", "§").replace("%HP%", String.valueOf(config.getInt("Potion.HP"))).replace("%Level%", String.valueOf(config.getInt("Potion.Level"))));
        	    	          potionmeta.setLore(lore);
        	    	        }
        	    	      }
        	    		potion.setItemMeta(potionmeta);
        	    		 p.getInventory().addItem(potion);
        	    		 p.updateInventory();
        	    		  return false;
        	    	  }else{
        	    		  p.sendMessage("§b§lAzarath §8§l» §cFejl: denne potion ser ikke ud til at findes!"); 
        	    	  }
        	    	  
        	      
        	      
        	      }
        	      
        	      
        
      }
    
	return false;
  }
}

package mc.frederik.højgaard.mobs;

import java.io.File;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Skeleton;
import org.bukkit.entity.Villager.Profession;
import org.bukkit.entity.Zombie;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

import com.gmail.filoghost.holographicdisplays.api.Hologram;
import com.gmail.filoghost.holographicdisplays.api.HologramsAPI;
import com.gmail.filoghost.holographicdisplays.api.handler.PickupHandler;
import com.gmail.filoghost.holographicdisplays.api.line.ItemLine;

import mc.frederik.højgaard.main;


public class zombie {

	
	public static void create(Entity e, String id, Player p){
	    //File file = new File(main.instance.getDataFolder().getPath(), "mobs/zombies/" + type + ".yml");
	   // YamlConfiguration config = YamlConfiguration.loadConfiguration(file);
	 //   for(String data : config.getConfigurationSection("Mob.Zombie").getKeys(false)){
		Zombie z = (Zombie)e;
		 Connection connect = null;
		 ResultSet res = null;
		 try {
			 connect = main.connection.getConnection();
			 
			 
			  res = connect.prepareStatement("SELECT * FROM mobs_zombies WHERE id='"+ id +"'").executeQuery();
			 if(res.next()){
			
					
					z.setCustomNameVisible(true);
					
					z.setCustomName(res.getString(2).replace("&", "§"));	
					z.setMaxHealth(res.getDouble(4));
					z.setHealth(res.getDouble(3));
					if(res.getBoolean(13) == true){
						if(res.getBoolean(41) == false){
						ItemStack helmet = new ItemStack(res.getInt(14));
						
					if(res.getBoolean(15) == true){ 						
					ItemMeta hm = helmet.getItemMeta();
					hm.spigot().setUnbreakable(true);
					
					helmet.setItemMeta(hm);
					}
					
					
					if(res.getBoolean(16) == true){
				LeatherArmorMeta meta = (LeatherArmorMeta) helmet.getItemMeta();
				Color color = Color.fromRGB(res.getInt(17), res.getInt(18), res.getInt(19));
				meta.setColor(color);
				helmet.setItemMeta(meta);
					}
					
					z.getEquipment().setHelmet(helmet);
					
						}else {
						ItemStack shelmet = new ItemStack(397,1,(short)3);
						SkullMeta meta = (SkullMeta) shelmet.getItemMeta();
						meta.setOwner(res.getString(42));
						shelmet.setItemMeta(meta);
						z.getEquipment().setHelmet(shelmet);
						
					}
					
					ItemStack chestplate = new ItemStack(res.getInt(20));
					if(res.getBoolean(21) == true){
					ItemMeta cm = chestplate.getItemMeta();
					cm.spigot().setUnbreakable(true);
					chestplate.setItemMeta(cm);
					
					}
					if(res.getBoolean(22) == true){
						LeatherArmorMeta meta = (LeatherArmorMeta) chestplate.getItemMeta();
						Color color = Color.fromRGB(res.getInt(23), res.getInt(24), res.getInt(25));
						meta.setColor(color);
						chestplate.setItemMeta(meta);
							}
					 z.getEquipment().setChestplate(chestplate);
					ItemStack leggings = new ItemStack(res.getInt(26));
					if(res.getBoolean(27) == true){
					ItemMeta lm = leggings.getItemMeta();
					lm.spigot().setUnbreakable(true);
					leggings.setItemMeta(lm);
					}
					if(res.getBoolean(28) == true){
						LeatherArmorMeta meta = (LeatherArmorMeta) leggings.getItemMeta();
						Color color = Color.fromRGB(res.getInt(29), res.getInt(30), res.getInt(31));
						meta.setColor(color);
						leggings.setItemMeta(meta);
							}
					
					 z.getEquipment().setLeggings(leggings);
					ItemStack boots = new ItemStack(res.getInt(32));
					if(res.getBoolean(33) == true){
					ItemMeta bm = boots.getItemMeta();
					bm.spigot().setUnbreakable(true);
					boots.setItemMeta(bm);
					}
					if(res.getBoolean(34) == true){
						LeatherArmorMeta meta = (LeatherArmorMeta) boots.getItemMeta();
						Color color = Color.fromRGB(res.getInt(35), res.getInt(36), res.getInt(37));
						meta.setColor(color);
						boots.setItemMeta(meta);
							}
					
			        z.getEquipment().setBoots(boots);
					
					}
					z.setBaby(res.getBoolean(38));
					if(res.getBoolean(39) == true){
					z.setVillagerProfession(Profession.NORMAL);
					z.setVillager(res.getBoolean(39));
					}
					
					ItemStack weapon = new ItemStack(res.getInt(40));
					ItemMeta wm = weapon.getItemMeta();
					//wm.spigot().setUnbreakable(true);
					weapon.setItemMeta(wm);
					z.getEquipment().setItemInHand(weapon);
				 
        	     res.close();
   
        		}else{
        	 z.setHealth(0.0D);	
			 p.sendMessage("§b§lAzarath §8§l» §cFejl: dette mob ser ikke ud til at findes!");
			 return;
        		}
			 
		} catch (SQLException ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
		}finally {
			if(connect != null){
				try {
					connect.close();
				} catch (SQLException ex) {
					// TODO Auto-generated catch block
					ex.printStackTrace();
				}
			}
			if(res != null){
				try {
					res.close();
				} catch (SQLException ex) {
					// TODO Auto-generated catch block
					ex.printStackTrace();
				}
			}
		}
	
		//e.getLocation(new Location(sender.getWorld(), sender.getLocation().getX(),sender.getLocation().getY(),sender.getLocation().getZ()));
	
		
	   // }
	}
	
	@EventHandler
	public static void DeathMessage(EntityDeathEvent event){
	
		if (event.getEntity() instanceof Zombie && event.getEntity().getKiller() instanceof Player){
		 event.getDrops().clear();
		 event.setDroppedExp(0);		 
		 if(event.getEntity().getKiller() instanceof Player){
			 
			 Connection connect = null;
			 ResultSet res = null;
			 Player p = (Player)event.getEntity().getKiller();
			 Entity z = (Entity)event.getEntity();
		
			 try {
				 connect = main.connection.getConnection();
				 

				  res = connect.prepareStatement("SELECT * FROM mobs_zombies WHERE name='"+ z.getCustomName().replace("§", "&") +"'").executeQuery();
				 if(res.next()){
			 

					 ItemStack icon = new ItemStack(Material.GOLD_INGOT);
					 Location loc2 = z.getLocation();
					 int y2 = loc2.getBlockY() + 1;
					  loc2.setY(y2);
					  
						final Hologram hologram = HologramsAPI.createHologram(main.instance, loc2);
						hologram.appendTextLine("§6Gold");
						ItemLine itemLine = hologram.appendItemLine(icon);

						itemLine.setPickupHandler(new PickupHandler() {	

							@Override
							public void onPickup(Player player) {
											p.playSound(p.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 2,2);
							p.sendMessage("test");
								hologram.delete();
											
							}
						});
					
					
			
			  Location loc = z.getLocation();
			  int y = loc.getBlockY() + 2;
			  loc.setY(y);
			  
		        Hologram holo2 =  HologramsAPI.createHologram(main.instance, loc);
		        holo2.appendTextLine("§7[§f+" + res.getInt(8) + " XP§7]");
		        holo2.appendTextLine("§7[" + p.getName() + "]");
		        holo2.getVisibilityManager().showTo(p);
		        holo2.getVisibilityManager().setVisibleByDefault(false);
		        res.close();
		        
			 Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(main.instance, new BukkitRunnable()
			    {
			      public void run()
			      {
			    	holo2.delete();
			        
			      }
			    
			    }, 40L);
		}
		
			} catch (SQLException ex) {
				// TODO Auto-generated catch block
				ex.printStackTrace();
			}finally {
				if(connect != null){
					try {
						connect.close();
					} catch (SQLException ex) {
						// TODO Auto-generated catch block
						ex.printStackTrace();
					}
				}
				if(res != null){
					try {
						res.close();
					} catch (SQLException ex) {
						// TODO Auto-generated catch block
						ex.printStackTrace();
					}
				
				}
			}
		 }
		}
		
		
		}
	
	
	
	
	@EventHandler
	public static void Damage(EntityDamageByEntityEvent event){
		
		if (event.getEntity() instanceof Player){

			Connection connect = null;
			ResultSet res = null;
			Player p = (Player)event.getEntity();
			Entity z = (Entity)event.getDamager();

			 try {
				 connect = main.connection.getConnection();
				 
				
				  res = connect.prepareStatement("SELECT * FROM mobs_zombies WHERE name='"+ z.getCustomName().replace("§", "&") +"'").executeQuery();
				 if(res.next()){

		
			
 	
				Random calc = new Random();
				int atk = calc.nextInt(res.getInt(5)) + res.getInt(6);
				event.setDamage(atk);
				p.sendMessage("Damage: " + event.getDamage());
			
				
				if(res.getBoolean(9) == true){	
					 p.addPotionEffect(new PotionEffect(PotionEffectType.getById(res.getInt(10)),res.getInt(11),res.getInt(12))); 
					 
				}
				res.close();
				 }
				 
				} catch (SQLException ex) {
					// TODO Auto-generated catch block
					ex.printStackTrace();
				}finally {
					if(connect != null){
						try {
							connect.close();
						} catch (SQLException ex) {
							// TODO Auto-generated catch block
							ex.printStackTrace();
						}
					}
					if(res != null){
						try {
							res.close();
						} catch (SQLException ex) {
							// TODO Auto-generated catch block
							ex.printStackTrace();
						}
					
					}
				}
			 }
			}
}

			
	
	
	
	
	
			

package mc.frederik.højgaard;

import java.sql.Connection;
import java.util.HashMap;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import com.zaxxer.hikari.HikariDataSource;

import mc.frederik.højgaard.admincmds.givearmor;
import mc.frederik.højgaard.admincmds.givehorse;
import mc.frederik.højgaard.admincmds.givepotion;
import mc.frederik.højgaard.admincmds.giveweapon;
import mc.frederik.højgaard.admincmds.spawnmob;
import mc.frederik.højgaard.events.register;
import mc.frederik.højgaard.events.register2;
import mc.frederik.højgaard.features.bar;
import mc.frederik.højgaard.features.scoreboard;

public class main extends JavaPlugin implements Listener{

	/*
	 * 
	 * 
	 * All rights reservs to hund35 | Frederik H
	 * 
	 * 
	 * 
	 */

	  public static main instance;
	  protected String user;
	  protected String password;
	  protected String db;
	  public static HikariDataSource connection;
	  public static Connection connect;
	  //public static Connection connect;

	  public static HashMap<Player, UUID> horsedata = new HashMap(); 
	  
	  public void onEnable()
	  {
	    instance = this;
	    user = "";
		password = "";
		db = "";
		
	    registerCommand();
	    Bukkit.getServer().getPluginManager().registerEvents(this, this);
	    Bukkit.getServer().getPluginManager().registerEvents(new register(), this);
	    Bukkit.getServer().getPluginManager().registerEvents(new register2(), this);
	   
	 scoreboard.load();
	 bar.load();
	
	 connection = new HikariDataSource();
	 connection.setMaximumPoolSize(10);
 	 connection.setJdbcUrl("jdbc:mysql://localhost:3306/" + db);
 	 connection.setUsername(user);
 	 connection.setPassword(password);
 	 
/*
 	try {
		connect = connection.getConnection();
		System.out.println("Connected");
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	  
	 */
	  }
	  
	 /*
	   try {
				connect = DriverManager.getConnection("jdbc:mysql://localhost:3306/" + db, user, password);
				System.out.println("Du er nu blevet tilsluttet til databasen!");
			} catch (SQLException e) {
				System.out.println("Kunne ikke connect til databasen!");
				e.printStackTrace();
			}
	    
	   */ 
	  
	  
	  public void onDisable(){
		  for(Player p : Bukkit.getOnlinePlayers()){
			  if(horsedata.containsKey(p)){
		  for(Entity e : p.getWorld().getEntities()){
			  if(e.getUniqueId().equals(horsedata.get(p))){
				  e.remove();
			  }
		  }
		  horsedata.remove(p);
		  }
		  }
	  }
	    
	  
	  private void registerCommand()
	  {
		  getCommand("spawnmob").setExecutor(new spawnmob());
		  getCommand("giveweapon").setExecutor(new giveweapon());
		  getCommand("givearmor").setExecutor(new givearmor());
		  getCommand("givepotion").setExecutor(new givepotion());
		  getCommand("givehorse").setExecutor(new givehorse());
	 
	  }
	
}

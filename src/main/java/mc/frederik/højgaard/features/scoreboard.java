package mc.frederik.højgaard.features;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import mc.frederik.højgaard.main;
import ru.tehkode.permissions.PermissionUser;
import ru.tehkode.permissions.bukkit.PermissionsEx;



public class scoreboard {

	@SuppressWarnings("deprecation")
	public static void load(){
	
    Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(main.instance, new BukkitRunnable()
    {
      public void run()
      {
        for (Player p : Bukkit.getOnlinePlayers()) {
        	sb(p);
        
        }
      }
    }, 0L, 120L);
  }
	
	 public static void sb(Player p)
	  {
	    Scoreboard board = Bukkit.getScoreboardManager().getNewScoreboard();
	    Objective ob = board.registerNewObjective("sb", "dummy");
	    
	    ob.setDisplaySlot(DisplaySlot.SIDEBAR);
	   
	    
	    ob.setDisplayName("§b§lAzarath");
	    ob.getScore("§cKommer snart").setScore(1);
	   // Score sc = ob.getScore("§cKommer snart");
	  
	    Team ejer = board.registerNewTeam("EJER");
	    Team dev = board.registerNewTeam("DEV");
	    Team admin = board.registerNewTeam("ADMIN");
	    Team mod = board.registerNewTeam("MOD");
	    Team gamedesign = board.registerNewTeam("GAMEDESIGN");

	    Team bygger = board.registerNewTeam("BYGGER");
	    Team trainee = board.registerNewTeam("TRAINEE");
	    Team yt = board.registerNewTeam("YOUTUBER");
	    Team king = board.registerNewTeam("KING");
	    Team legend = board.registerNewTeam("LEGEND");
	    Team hero = board.registerNewTeam("HERO");
	    
	    ejer.setPrefix("§d§lEJER §f");
		   dev.setPrefix("§5§lDEV §f");
		   admin.setPrefix("§4§lADMIN §f");
		   mod.setPrefix("§6§lMOD §f");
		   
		   gamedesign.setPrefix("§9§lGD §f");
		   bygger.setPrefix("§9§lBYGGER §f");
		   trainee.setPrefix("§e§lTRAINEE §f");
		   yt.setPrefix("§c§lY§f§lT §f");
		   king.setPrefix("§c§lKING §f");
		   legend.setPrefix("§b§lLEGEND §f");
		   hero.setPrefix("§a§lHERO §f");
	    
	
	
	   
	   
	    for(Player players : Bukkit.getOnlinePlayers()){
	    	 PermissionUser user = PermissionsEx.getUser(players);
	    	 
	    	 if(user.inGroup("Ejer")){   
	        	 ejer.addPlayer(players);
	        	 
	    	}else if(user.inGroup("Dev")){
	    		dev.addPlayer(players);
	    	}else if(user.inGroup("Admin")){
	    		admin.addPlayer(players);
	    	}else if(user.inGroup("Mod")){
	    		mod.addPlayer(players);
	    	}else if(user.inGroup("GameDesign")){
	    		gamedesign.addPlayer(players);
	    	}else if(user.inGroup("Bygger")){
	    		bygger.addPlayer(players);
	    	}else if(user.inGroup("Trainee")){
	    		trainee.addPlayer(players);
	    	}else if(user.inGroup("Youtuber")){
	    		yt.addPlayer(players);
	    	
	    	}else if(user.inGroup("King")){
	    		king.addPlayer(players);
	         }else if(user.inGroup("Legend")){
	        	 legend.addPlayer(players);
	         }else if(user.inGroup("Hero")){
	        	 hero.addPlayer(players);
	    	  }else{
	    		  
	    	  }
	    
	    
	    
	    p.setScoreboard(board);
	  }
	  }
	
}